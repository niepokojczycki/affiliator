<?php
use App\Support\Money;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 24.03.17
 * Time: 18:22
 */
class MoneyTest extends Tests\TestCase
{

    public function test_inDollars_returns_correctly_formatted_value()
    {
        $amount = 1234;
        $money = new Money($amount);

        $this->assertEquals('$12.34', $money->inDollars());
    }

    public function test_inDollars_returns_correct_thousand_separator()
    {
        $amount = 123456;
        $money = new Money($amount);

        $this->assertEquals('$1,234.56', $money->inDollars());
    }

    public function test_add_mutates_the_amount()
    {
        $amount = 123456;
        $money = new Money($amount);

        $added = 1234;
        $money->add(Money::fromCents($added));

        $this->assertEquals($money->getAmount(), $amount + $added);
    }

    public function test_subtract_mutates_the_amount()
    {
        $amount = 123456;
        $money = new Money($amount);

        $added = 1234;
        $money->subtract(Money::fromCents($added));

        $this->assertEquals($money->getAmount(), $amount - $added);
    }

    public function test_subtract_throws_exception_if_new_amount_is_negative()
    {
        $this->expectException(Exception::class);

        $amount = 123456;
        $money = new Money($amount);

        $added = 1234567;
        $money->subtract(Money::fromCents($added));
    }
}