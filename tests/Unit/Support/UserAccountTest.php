<?php
use App\Entities\User;
use App\Support\Money;
use App\Support\UserAccount;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 05.04.17
 * Time: 11:35
 */
class UserAccountTest extends Tests\TestCase
{

    protected $cents;
    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->cents = 10000;
        $this->user = factory(User::class)->create(['cash_available' => $this->cents]);
    }

    public function test_balance_returns_instance_of_money()
    {
        $account = new UserAccount($this->user);

        $this->assertInstanceOf(Money::class, $account->balance());
    }

    public function test_balance_returns_correct_amount_of_money()
    {
        $account = new UserAccount($this->user);
        $expected = $account->balance()->getAmount();
        $actual = $this->cents;

        $this->assertEquals($expected, $actual);
    }

    public function test_withdraw_subtracts_money_from_account()
    {
        $subtractAmount = 1000;
        $expected = $this->cents - $subtractAmount;
        $account = new UserAccount($this->user);
        $account->withdraw(Money::fromCents($subtractAmount));
        $actual = $account->balance()->getAmount();

        $this->assertEquals($expected, $actual);
    }

    public function test_withdraw_saves_users_balance()
    {
        $subtractAmount = 1000;
        $expected = $this->cents - $subtractAmount;
        $account = new UserAccount($this->user);
        $account->withdraw(Money::fromCents($subtractAmount));
        $actual = $this->user->fresh()->account()->balance()->getAmount();

        $this->assertEquals($expected, $actual);
    }

    public function test_add_adds_money_from_account()
    {
        $subtractAmount = 1000;
        $expected = $this->cents + $subtractAmount;
        $account = new UserAccount($this->user);
        $account->add(Money::fromCents($subtractAmount));
        $actual = $account->balance()->getAmount();

        $this->assertEquals($expected, $actual);
    }

    public function test_add_saves_users_balance()
    {
        $subtractAmount = 1000;
        $expected = $this->cents + $subtractAmount;
        $account = new UserAccount($this->user);
        $account->add(Money::fromCents($subtractAmount));
        $actual = $this->user->fresh()->account()->balance()->getAmount();

        $this->assertEquals($expected, $actual);
    }
}