<?php
use App\Support\Money;
use App\ViewModels\DashboardElement;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 30.03.17
 * Time: 14:10
 */
class DashboardElementTest extends Tests\TestCase
{

    public function test_presentCount_will_return_monetary_value_if_necessary()
    {
        $value = 1000;
        $topElement = new DashboardElement('name', $value, false, true);

        $this->assertEquals($topElement->presentCount(), (new Money($value))->inDollars());
    }

    public function test_presentCount_will_add_suffix_when_weekly()
    {
        $count = 1000;
        $topElement = new DashboardElement('name', $count);

        $this->assertEquals($topElement->presentCount(), "{$count} / week");
    }

}