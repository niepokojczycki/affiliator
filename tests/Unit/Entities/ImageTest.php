<?php
use App\Entities\Image;
use App\Entities\User;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 24.03.17
 * Time: 16:41
 */
class ImageTest extends Tests\TestCase
{

    use InteractsWithDatabase;

    /** @var  UploadedFile */
    private $file;

    private $user;

    public function setUp()
    {
        parent::setUp();

        $this->file = $this->setupImageFile();
        $this->user = factory(User::class)->create();
    }

    public function test_createFileWithPhoto_returns_new_image_object()
    {
        $image = Image::createWithFile($this->user, $this->file);

        $this->assertInstanceOf(Image::class, $image);

        $this->deleteImage($image);
    }

    public function test_createFileWithPhoto_persists_image_in_database()
    {
        $image = Image::createWithFile($this->user, $this->file);

        $this->assertDatabaseHas('images', [
            'original_name' => $this->file->getClientOriginalName()
        ]);

        $this->deleteImage($image);
    }

    public function test_createFileWithPhoto_assigns_created_image_to_target()
    {
        $image = Image::createWithFile($this->user, $this->file);

        $this->assertEquals($image->target, $this->user);

        $this->deleteImage($image);
    }

    protected function setupImageFile()
    {
        $name = 'test_image';
        $ext = 'jpeg';
        $path = storage_path('' . $name . '.' . $ext);

        return new UploadedFile($path, $name, 'image/jpeg', 1234, null, true);
    }

    protected function deleteImage(Image $image)
    {
        File::delete(public_path($image->path));
    }
}