<?php
use App\Entities\Product;
use App\Support\Money;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 24.03.17
 * Time: 18:22
 */
class ProductTest extends Tests\TestCase
{

    public function test_setting_Money_object_as_payout_converts_value_to_integer()
    {
        $product = factory(Product::class)->create();

        $amount = 2137;

        $money = new Money($amount);

        $product->payout = $money;
        $product->save();

        $this->assertEquals($product->payout, $money->getAmount());
    }

    public function test_payout_method_will_return_money_object()
    {
        $amount = 2137;

        $product = factory(Product::class)->create(
            ['payout' => $amount]
        );

        $payout = $product->payout();

        $this->assertInstanceOf(Money::class, $payout);
        $this->assertEquals($payout->getAmount(), $product->payout);
    }
}