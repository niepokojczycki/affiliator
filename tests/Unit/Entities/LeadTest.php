<?php
use App\Entities\Lead;
use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 30.03.17
 * Time: 13:58
 */
class LeadTest extends Tests\TestCase
{

    public function test_isSold_will_return_boolean()
    {
        $lead = new Lead;
        $this->assertInternalType('boolean', $lead->isSold());
    }


    public function test_isSold_will_return_true_if_lead_is_sold()
    {
        $lead = new Lead(['status' => Lead::SOLD]);
        $this->assertTrue($lead->isSold());
    }

    public function test_isSold_will_return_false_if_lead_is_rejected()
    {
        $lead = new Lead(['status' => Lead::REJECTED]);
        $this->assertFalse($lead->isSold());
    }

    public function test_isSold_will_return_false_if_lead_is_awaiting()
    {
        $lead = new Lead(['status' => Lead::AWAITING]);
        $this->assertFalse($lead->isSold());
    }
    
    public function test_isAwaiting_will_return_true_if_lead_is_awaiting()
    {
        $lead = new Lead(['status' => Lead::AWAITING]);
        $this->assertTrue($lead->isAwaiting());
    }

    public function test_isAwaiting_will_return_false_if_lead_is_rejected()
    {
        $lead = new Lead(['status' => Lead::REJECTED]);
        $this->assertFalse($lead->isAwaiting());
    }

    public function test_isAwaiting_will_return_false_if_lead_is_sold()
    {
        $lead = new Lead(['status' => Lead::SOLD]);
        $this->assertFalse($lead->isAwaiting());
    }
    
    public function test_isRejected_will_return_true_if_lead_is_rejected()
    {
        $lead = new Lead(['status' => Lead::REJECTED]);
        $this->assertTrue($lead->isRejected());
    }

    public function test_isRejected_will_return_false_if_lead_is_sold()
    {
        $lead = new Lead(['status' => Lead::SOLD]);
        $this->assertFalse($lead->isRejected());
    }

    public function test_isRejected_will_return_false_if_lead_is_awaiting()
    {
        $lead = new Lead(['status' => Lead::AWAITING]);
        $this->assertFalse($lead->isRejected());
    }

    public function test_isBetweenDates_will_return_true_if_created_at_is_between_dates()
    {
        $now = Carbon::now();

        $lead = new Lead;
        $lead->created_at = $now;

        $this->assertTrue($lead->isBetweenDates($now->copy()->subDay(), $now->copy()->addDay()));
    }

    public function test_isBetweenDates_will_return_false_if_created_at_is_out_of_dates()
    {
        $now = Carbon::now();

        $lead = new Lead;
        $lead->created_at = $now;

        $this->assertFalse($lead->isBetweenDates($now->copy()->addDay(), $now->copy()->addDays(2)));
        $this->assertFalse($lead->isBetweenDates($now->copy()->subDays(2), $now->copy()->subDay()));
    }
}