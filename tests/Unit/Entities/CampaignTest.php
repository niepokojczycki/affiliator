<?php
use App\Entities\Campaign;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 03.04.17
 * Time: 15:33
 */
class CampaignTest extends Tests\TestCase
{

    public function test_updateEPC_returns_null_if_campaign_has_no_clicks()
    {
        $campaign = factory(Campaign::class)->create();

        $this->assertNull($campaign->updateEPC());
    }
}






