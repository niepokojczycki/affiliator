<?php
use App\Entities\User;
use App\Repositories\CampaignRepository;
use App\Services\CampaignGroupStatistics;
use App\Services\Dashboard;
use App\Support\Money;
use App\ViewModels\DashboardElement;
use Carbon\Carbon;
use Illuminate\Support\Collection;


/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 30.03.17
 * Time: 13:25
 */
class DashboardTest extends Tests\TestCase
{

    /** @var  Dashboard */
    protected $dashboard;
    /** @var  CampaignRepository|PHPUnit_Framework_MockObject_MockObject */
    protected $mockRepository;
    /** @var  CampaignGroupStatistics|PHPUnit_Framework_MockObject_MockObject */
    protected $mockStatistics;

    public function setUp()
    {
        parent::setUp();

        $user = factory(User::class)->create();
        $this->be($user);

        $this->mockRepository = $this->getMockBuilder(CampaignRepository::class)->getMock();
    }

    public function test_getMainChartLabels_returns_array()
    {
        $mockStatistics = $this->getMockStatistics();
        $dashboard = new Dashboard($this->mockRepository, $mockStatistics);

        $this->assertInternalType("array", $dashboard->getMainChartLabels());
    }

    public function test_getMainChartLabels_returns_values_for_seven_days()
    {

        $mockStatistics = $this->getMockStatistics();
        $dashboard = new Dashboard($this->mockRepository, $mockStatistics);

        $expected = 7;
        $actual = $dashboard->getMainChartLabels();

        $this->assertCount($expected, $actual);
    }

    public function test_getMainChartLabels_returns_correct_values()
    {

        $mockStatistics = $this->getMockStatistics();
        $dashboard = new Dashboard($this->mockRepository, $mockStatistics);


        $expected = Carbon::now()->format('d.m');
        $actual = $dashboard->getMainChartLabels()[6];

        $this->assertEquals($expected, $actual);

        $expected = Carbon::now()->subDays(6)->format('d.m');
        $actual = $dashboard->getMainChartLabels()[0];

        $this->assertEquals($expected, $actual);
    }

    public function test_getRevenueCountByDay_returns_array()
    {
        $mockStatistics = $this->getMockStatistics();
        $dashboard = new Dashboard($this->mockRepository, $mockStatistics);

        $this->assertInternalType("array", $dashboard->getRevenueCountByDay());
    }

    public function test_getRevenueCountByDay_returns_values_for_seven_days()
    {
        $mockStatistics = $this
            ->getMockBuilder(CampaignGroupStatistics::class)
            ->setMethods(['totalRevenue'])
            ->disableOriginalConstructor()
            ->getMock();

        $mockStatistics
            ->expects($this->exactly(7))
            ->method('totalRevenue')
            ->willReturn(new Money(123));

        $dashboard = new Dashboard($this->mockRepository, $mockStatistics);

        $expected = 7;

        $actual = $dashboard->getRevenueCountByDay();

        $this->assertCount($expected, $actual);
    }


    public function test_getDashboardElements_returns_a_support_collection()
    {
        $mockStatistics = $this->buildMockForDashboardElementsTests();
        $dashboard = new Dashboard($this->mockRepository, $mockStatistics);

        $actual = $dashboard->getDashboardElements();
        $expected = Collection::class;

        $this->assertInstanceOf($expected, $actual);
    }

    public function test_getDashboardElements_returns_a_collection_of_TopElements()
    {
        $mockStatistics = $this->buildMockForDashboardElementsTests();
        $dashboard = new Dashboard($this->mockRepository, $mockStatistics);

        $elements = $dashboard->getDashboardElements();
        $expected = DashboardElement::class;

        foreach ($elements as $actual)
            $this->assertInstanceOf($expected, $actual);
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    private function buildMockForDashboardElementsTests(): PHPUnit_Framework_MockObject_MockObject
    {
        $mockStatistics = $this->getMockBuilder(CampaignGroupStatistics::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'totalRevenue',
                'clicks',
                'sales'
            ])
            ->getMock();

        $mockStatistics
            ->expects($this->exactly(2))
            ->method('totalRevenue')
            ->willReturn(new Money(123));

        $mockStatistics
            ->expects($this->once())
            ->method('clicks')
            ->willReturn(1);

        $mockStatistics
            ->expects($this->once())
            ->method('sales')
            ->willReturn(1);
        return $mockStatistics;
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    private function getMockStatistics(): PHPUnit_Framework_MockObject_MockObject
    {
        $mockStatistics = $this->getMockBuilder(CampaignGroupStatistics::class)
            ->disableOriginalConstructor()->getMock();
        return $mockStatistics;
    }
}