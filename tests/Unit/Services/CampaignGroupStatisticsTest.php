<?php
use App\Entities\CampaignClick;
use App\Entities\Lead;
use App\Services\CampaignGroupStatistics;
use App\Support\Money;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 04.04.17
 * Time: 09:08
 */
class CampaignGroupStatisticsTest extends Tests\TestCase
{

    protected $user;
    protected $campaigns;
    /** @var  CampaignGroupStatistics */
    protected $groupStatistics;
    protected $leadsCount;
    protected $leadPrice;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(\App\Entities\User::class)->create();
        $this->be($this->user);

        $this->campaigns = factory(\App\Entities\Campaign::class, 3)->create([
            'user_id' => $this->user->id
        ]);

        $this->leadPrice = 100;
        $this->leadsCount = 0;
        $carbon = \Carbon\Carbon::now()->startOfDay()->addHours(3);
        $numberOfLeads = 3;

        foreach ($this->campaigns as $campaign) {

            factory(Lead::class, $numberOfLeads)->create([
                'campaign_id' => $campaign->id,
                'created_at'  => $carbon,
                'price'       => $this->leadPrice
            ]);

            $this->leadsCount += $numberOfLeads;

            factory(CampaignClick::class, 3)->create([
                'campaign_id'  => $campaign->id,
                'clicks_count' => random_int(10, 100),
                'timestamp'    => $carbon->timestamp]);
        }

        $this->groupStatistics = (new CampaignGroupStatistics($this->campaigns));
    }

    public function test_clicks_will_return_aggregated_click_count()
    {
        $expected = $this->campaigns->map->clicks->flatten()->sum('clicks_count');
        $actual = $this->groupStatistics->clicks();

        $this->assertEquals($expected, $actual);
    }

    public function test_leads_will_return_aggregated_leads_count()
    {
        $expected = $this->leadsCount;
        $actual = $this->groupStatistics->leads();

        $this->assertEquals($expected, $actual);
    }

    public function test_sales_will_return_aggregated_sold_leads_count()
    {
        $expected = $this->campaigns->map->leads->flatten()->filter->isSold()->count();
        $actual = $this->groupStatistics->sales();
        $this->assertEquals($expected, $actual);
    }

    public function test_awaiting_will_return_aggregated_sold_leads_count()
    {
        $expected = $this->campaigns->map->leads->flatten()->filter->isAwaiting()->count();
        $actual = $this->groupStatistics->awaiting();
        $this->assertEquals($expected, $actual);
    }

    public function test_rejects_will_return_aggregated_sold_leads_count()
    {
        $expected = $this->campaigns->map->leads->flatten()->filter->isRejected()->count();
        $actual = $this->groupStatistics->rejects();
        $this->assertEquals($expected, $actual);
    }

    public function test_salesRevenue_will_return_Money()
    {
        $this->assertInstanceOf(Money::class, $this->groupStatistics->salesRevenue());
    }

    public function test_holdsRevenue_will_return_Money()
    {
        $this->assertInstanceOf(Money::class, $this->groupStatistics->holdsRevenue());
    }

    public function test_leadsRevenue_will_return_Money()
    {
        $this->assertInstanceOf(Money::class, $this->groupStatistics->leadsRevenue());
    }

    public function test_totalRevenue_will_return_Money()
    {
        $this->assertInstanceOf(Money::class, $this->groupStatistics->totalRevenue());
    }

    public function test_totalRevenue_will_return_sum_of_revenues()
    {
        $sales = $this->groupStatistics->salesRevenue()->getAmount();
        $holds = $this->groupStatistics->holdsRevenue()->getAmount();
        $leads = $this->groupStatistics->leadsRevenue()->getAmount();

        $expected = new Money($sales + $holds + $leads);
        $actual = $this->groupStatistics->totalRevenue();

        $this->assertEquals($expected, $actual);
    }

    public function test_salesRevenue_will_return_sum_of_sold_leads_prices()
    {
        $leadsCount = $this->campaigns->map->leads->flatten()->filter->isSold()->count();
        $expected = new Money($leadsCount * $this->leadPrice);
        $actual = $this->groupStatistics->salesRevenue();

        $this->assertEquals($expected, $actual);
    }
}
