<?php
use App\Adapters\DashboardAdapter;
use App\Http\Controllers\DashboardController;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 31.03.17
 * Time: 15:59
 */
class DashboardControllerTest extends Tests\TestCase
{

    public function test_show_method_passes_data_from_adapter_show_method()
    {
        $returnValue = "some value";

        $mock  = $this->createMock(DashboardAdapter::class);
        $mock->method('show')->willReturn($returnValue);

        $controller = new DashboardController();

        $this->assertEquals($returnValue, $controller->show($mock));
    }
}