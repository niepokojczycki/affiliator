<?php
use App\Adapters\DashboardAdapter;
use App\Services\Dashboard;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 31.03.17
 * Time: 16:06
 */
class DashboardAdapterTest extends Tests\TestCase
{

    public function test_show_method_returns_instance_of_view()
    {
        $adapter = $this->mockDashboard();
        $this->assertInstanceOf(View::class, $adapter->show());
    }

    public function test_getAjaxForMainChart_method_will_return_instance_of_response()
    {
        $adapter = $this->mockDashboard();
        $this->assertInstanceOf(JsonResponse::class, $adapter->getAjaxForMainChart());
    }

    private function mockDashboard(): DashboardAdapter
    {
        /** @var Dashboard|PHPUnit_Framework_MockObject_MockObject $mockDashboard */
        $mockDashboard = $this->createMock(Dashboard::class);

        return new DashboardAdapter($mockDashboard);
    }

}