<?php

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 28.03.17
 * Time: 18:14
 */
class CampaignObserverTest extends Tests\TestCase
{
    public function test_on_creating_campaign_will_get_uuid()
    {
        $campaign = new \App\Entities\Campaign();
        $observer = new \App\Observers\CampaignObserver();

        $observer->creating($campaign);

        $this->assertTrue($this->validateUuid4($campaign->uuid));
    }

    protected function validateUuid4($value)
    {
        $UUIDv4 = '/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i';
        return boolval(preg_match($UUIDv4, $value));
    }

}