<?php
use App\Entities\Product;
use App\Entities\ProductCategory;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 27.03.17
 * Time: 17:10
 */
class ProductPresenterTest extends Tests\TestCase
{

    public function test_categoriesCount_returns_category_name_if_product_has_one_category()
    {
        $category = factory(ProductCategory::class)->create();
        $product = factory(Product::class)->create();

        $product->categories()->attach($category);
        $product->save();

        $expected = $category->name;
        $actual = $product->present()->categoriesCount();

        $this->assertEquals($expected, $actual);
    }


    public function test_categoriesCount_returns_info_about_categories_count_if_more_than_one()
    {
        $categories = factory(ProductCategory::class, 3)->create();
        $product = factory(Product::class)->create();

        $product->categories()->sync($categories);
        $product->save();

        $expected = $categories->count() . " categories";
        $actual = $product->present()->categoriesCount();

        $this->assertEquals($expected, $actual);
    }

    public function test_categoriesCount_returns_info_when_product_has_no_categories()
    {
        $product = factory(Product::class)->create();

        $expected = "No category";
        $actual = $product->present()->categoriesCount();

        $this->assertEquals($expected, $actual);
    }
}