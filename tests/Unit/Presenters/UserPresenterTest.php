<?php

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 27.03.17
 * Time: 17:10
 */
class UserPresenterTest extends Tests\TestCase
{
    public function test_fullName_method_returns_correct_value()
    {
        $user = factory(\App\Entities\User::class)->make();

        $firstName = $user->first_name;
        $lastName = $user->last_name;

        $this->assertEquals($user->present()->fullName(), $firstName. ' '. $lastName);
    }
}