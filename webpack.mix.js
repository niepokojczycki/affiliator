const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/campaign_form.js', 'public/js')
    .js('resources/assets/js/copy_to_clipboard.js', 'public/js')
    .js('resources/assets/js/dashboard_charts.js', 'public/js')
    .js('resources/assets/js/landing.js', 'public/js')
    .js('resources/assets/js/payout.js', 'public/js')
    .js('resources/assets/js/prelanding.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css');
