<?php
use App\Entities\Campaign;
use App\Entities\Lead;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 30.03.17
 * Time: 12:11
 */
class LeadsTableSeeder extends Seeder
{

    public function run()
    {
        $campaign = Campaign::first();

        $leads = factory(Lead::class, 100)->make();

        foreach ($leads as $lead) {
            $lead->campaign()->associate($campaign);
            $lead->price = $campaign->product->payout;
            $lead->save();
        }
    }

}