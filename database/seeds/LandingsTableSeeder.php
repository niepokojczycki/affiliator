<?php
use App\Entities\Landing;
use App\Entities\Product;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 28.03.17
 * Time: 12:32
 */
class LandingsTableSeeder extends Seeder
{

    public function run()
    {
        $adjectives = ['red', 'white', 'blue'];

        foreach (Product::all() as $product) {

            $landings = factory(Landing::class, 3)->make();

            foreach ($landings as $landing) {
                $landing->name = $product->name . ' - ' . $adjectives[array_rand($adjectives)];
                $landing->product()->associate($product);
                $landing->save();
            }
        }
    }
}