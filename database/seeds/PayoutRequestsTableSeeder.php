<?php
use App\Entities\PaymentMethod;
use App\Entities\PayoutRequest;
use App\Entities\User;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 05.04.17
 * Time: 12:54
 */
class PayoutRequestsTableSeeder extends Seeder
{

    public function run()
    {
        $payoutMethod = PaymentMethod::first();
        $user = User::first();

        factory(PayoutRequest::class, 100)->create([
            'payment_method_id' => $payoutMethod->id,
            'user_id'           => $user->id
        ]);

    }
}