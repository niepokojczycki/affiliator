<?php
use App\Entities\PaymentMethod;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 04.04.17
 * Time: 15:58
 */
class PaymentMethodsTableSeeder extends Seeder
{

    public function run()
    {
        factory(PaymentMethod::class, 3)->create();
    }
}