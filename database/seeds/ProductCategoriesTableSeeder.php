<?php

use App\Entities\Country;
use App\Entities\Product;
use App\Entities\ProductCategory;
use App\Repositories\ProductRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

class ProductCategoriesTableSeeder extends Seeder
{
    public function run(ProductRepository $productRepository)
    {
        /** @var Collection $categories */
        $categories = factory(ProductCategory::class, 3)->create();

        $products = $productRepository->all();

        foreach ($products as $product) {
            /** @var Product $product */
            $product->categories()->attach($categories->random());
            $product->save();
        }
    }
}
