<?php

use App\Entities\Image;
use App\Entities\Product;
use Illuminate\Database\Seeder;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 27.03.17
 * Time: 16:20
 */
class ImagesTableSeeder extends Seeder
{
    public function run()
    {
        $name = 'test_image';
        $ext = 'jpeg';
        $path = storage_path('' . $name . '.' . $ext);

        $file = new UploadedFile($path, $name, 'image/jpeg', 1234, null, true);

        $product = Product::first();

        Image::createWithFile($product, $file);
    }
}