<?php

use App\Entities\Product;
use App\Entities\User;
use Illuminate\Database\Seeder;

class CampaignsTableSeeder extends Seeder
{
    public function run()
    {
        $product = Product::first();
        $users = User::all();

        $data = [
            'postback_leads'   => 'https://www.google.pl/?leads=true',
            'postback_holds'   => 'https://www.google.pl/?holds=true',
            'postback_rejects' => 'https://www.google.pl/?rejects=true',
            'pixel_id'         => rand(100000000, 10000000000),
        ];

        foreach ($users as $user) {
            $campaign = new \App\Entities\Campaign($data);

            $campaign->user()->associate($user);
            $campaign->product()->associate($product);

            $campaign->save();

            foreach ($product->landings as $landing)
                $campaign->landings()->attach($landing);

            foreach ($product->prelandings as $prelanding)
                $campaign->prelandings()->attach($prelanding);

            $campaign->save();
        }
    }
}
