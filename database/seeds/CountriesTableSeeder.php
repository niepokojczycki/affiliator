<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder {

    private $countryRepository;

    public function __construct(\App\Repositories\CountryRepository $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries_json = json_decode(\Illuminate\Support\Facades\File::get(storage_path('app/countries.json')));

        foreach ($countries_json as $country)
        {
            if (!empty($country->currency_code) && !empty($country->iso_3166_2))
            {
                if (!$this->countryRepository->findByField('name', $country->name)->first())
                {
                    $this->countryRepository->create([
                        'name'     => $country->name,
                        'symbol'   => $country->iso_3166_2,
                        'currency' => $country->currency_code,
                    ]);
                }
            }
        }
    }
}
