<?php
use App\Entities\Landing;
use App\Entities\Prelanding;
use App\Entities\Product;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 28.03.17
 * Time: 12:32
 */
class PrelandingsTableSeeder extends Seeder
{

    public function run()
    {
        $adjectives = ['red', 'white', 'blue'];

        foreach (Product::all() as $product) {

            $prelandings = factory(Prelanding::class, 3)->make();

            foreach ($prelandings as $prelanding) {
                $prelanding->name = $product->name . ' - ' . $adjectives[array_rand($adjectives)];
                $prelanding->product()->associate($product);
                $prelanding->save();
            }
        }

    }
}