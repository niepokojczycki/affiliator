<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
//        $this->call(ProductsTableSeeder::class);
        $this->call(ProductCategoriesTableSeeder::class);
//        $this->call(ImagesTableSeeder::class);
//        $this->call(LandingsTableSeeder::class);
//        $this->call(PrelandingsTableSeeder::class);
//        $this->call(CampaignsTableSeeder::class);
//        $this->call(LeadsTableSeeder::class);
        $this->call(PaymentMethodsTableSeeder::class);
//        $this->call(PayoutRequestsTableSeeder::class);
    }
}
