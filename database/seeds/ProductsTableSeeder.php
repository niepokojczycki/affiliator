<?php

use App\Entities\Country;
use App\Entities\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    public function run()
    {
        $products = factory(Product::class, 100)->make();

        $countries = Country::whereIn('id', [1,2,3,172])->get();

        foreach ($products as $product) {
            /** @var Product $product */
            $product->country()->associate($countries->random());
            $product->save();
        }
    }
}
