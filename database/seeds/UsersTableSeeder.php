<?php

use App\Entities\Country;
use App\Entities\User;
use App\Repositories\UserRepository;
use App\Services\RegularUserService;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var RegularUserService
     */
    private $regularUserService;

    public function __construct(UserRepository $userRepository, RegularUserService $regularUserService)
    {
        $this->userRepository = $userRepository;
        $this->regularUserService = $regularUserService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = collect();

//        $users->push([
//            'email'      => 'info@gmatkowski.pl',
//            'password'   => 'test123',
//            'role'       => User::ADMIN,
//            'first_name' => 'Grzegorz',
//            'last_name'  => 'Matkowski',
//            'city'       => 'Mosina',
//            'state'      => 'Wielkopolskie',
//            'postal'     => '62-050',
//            'address'    => 'Os. Miodowe 6/16',
//            'phone'      => '517936131',
//            'active'     => true
//        ]);
//
//        $users->push([
//            'email'      => 'dimekg@gmail.com',
//            'password'   => 'test123',
//            'role'       => User::ADMIN,
//            'first_name' => 'Dmitr',
//            'last_name'  => 'Gluszczenko',
//            'city'       => 'Warszawa',
//            'state'      => 'Mazowieckie',
//            'postal'     => '01-001',
//            'address'    => 'Wołodyjowskiego 40a',
//            'phone'      => '555666622',
//            'active'     => true
//        ]);

        $users->push([
            'email'          => '123@123.pl',
            'password'       => '123123',
            'role'           => User::ADMIN,
            'first_name'     => 'Krzysiek',
            'last_name'      => 'Niepokojczykcki',
            'city'           => 'Warszawa',
            'state'          => 'Mazowieckie',
            'postal'         => '01-001',
            'address'        => 'Wołodyjowskiego 40a',
            'phone'          => '555666622',
            'active'         => true,
            'cash_available' => 12345678
        ]);

        $country = Country::where('name', 'Poland')->first();

        foreach ($users as $userData) {

            if ($this->userRepository->findByField('email', array_get($userData, 'email'))->count() == 0) {
                $user = $this->regularUserService->register($userData, true);
                $user->forceFill(array_except($userData, ['name', 'email', 'password']))->toArray();

                $user->country()->associate($country);
                $user->save();
            }
        }

    }
}
