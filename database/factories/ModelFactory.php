<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Entities\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'first_name' => $faker->firstName,
        'last_name'  => $faker->lastName,
        'email'      => $faker->unique()->safeEmail,
        'password'   => $password ?: $password = bcrypt('secret'),
        'role'       => 'C',
        'city'       => $faker->city,
        'state'      => $faker->city,
        'postal'     => $faker->postcode,
        'address'    => $faker->streetAddress,
        'active'     => true,
        'token'      => $faker->md5,
    ];
});


$factory->define(\App\Entities\Campaign::class, function (\Faker\Generator $faker) {
    $user = factory(\App\Entities\User::class)->create();
    $product = factory(\App\Entities\Product::class)->create();

    return [
        'user_id'    => $user->id,
        'product_id' => $product->id,
        'epc'        => 0.0
    ];
});

$factory->define(\App\Entities\Country::class, function (\Faker\Generator $faker) {

    return [
        'name'     => $faker->country,
        'symbol'   => $faker->countryCode,
        'currency' => $faker->currencyCode
    ];
});

$factory->define(\App\Entities\Product::class, function (\Faker\Generator $faker) {

    return [
        'name'        => $faker->word,
        'description' => implode('. ', $faker->sentences(3)),
        'payout'      => $faker->numberBetween(100, 1000),
        'country_id'  => factory(\App\Entities\Country::class)->create()->id
    ];
});

$factory->define(\App\Entities\ProductCategory::class, function (\Faker\Generator $faker) {
    return [
        'name' => $faker->colorName
    ];
});

$factory->define(\App\Entities\Landing::class, function (\Faker\Generator $faker) {
    return [
        'name' => $faker->firstNameFemale
    ];
});

$factory->define(\App\Entities\Prelanding::class, function (\Faker\Generator $faker) {
    return [
        'name' => $faker->firstNameMale
    ];
});

$factory->define(\App\Entities\Lead::class, function (\Faker\Generator $faker) {
    return [
        'status' => random_int(\App\Entities\Lead::AWAITING, \App\Entities\Lead::SOLD),
    ];
});

$factory->define(\App\Entities\CampaignClick::class, function (\Faker\Generator $faker) {

    $campaign = factory(\App\Entities\Campaign::class)->create();

    return [
        'campaign_id'  => $campaign->id,
        'timestamp'    => \Carbon\Carbon::now()->timestamp,
        'clicks_count' => rand(1, 200)
    ];
});

$factory->define(\App\Entities\PaymentMethod::class, function (\Faker\Generator $faker) {

    return [
        'name'           => $faker->firstNameFemale,
        'description'    => $faker->sentence(6),
        'minimum_amount' => random_int(500, 5000),
        'commission'     => random_int(100, 500),
    ];
});

$factory->define(\App\Entities\PayoutRequest::class, function (\Faker\Generator $faker) {

    return [
        'amount'            => random_int(10000, 100000),
        'status'            => \App\Entities\PayoutRequest::PAID,
    ];
});