<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');

            $table->uuid('uuid');
            $table->float('epc', 8, 6)->unsigned()->default(0.0);

            $table->string('postback_leads')->nullable();
            $table->string('postback_holds')->nullable();
            $table->string('postback_rejects')->nullable();

            $table->bigInteger('pixel_id')->nullable();
            $table->text('retarget_code')->nullable();

            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('campaign_landing', function (Blueprint $table) {
            $table->integer('campaign_id')->unsigned();
            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('cascade');

            $table->integer('landing_id')->unsigned();
            $table->foreign('landing_id')->references('id')->on('landings')->onDelete('cascade');
        });

        Schema::create('campaign_prelanding', function (Blueprint $table) {
            $table->integer('campaign_id')->unsigned();
            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('cascade');

            $table->integer('prelanding_id')->unsigned();
            $table->foreign('prelanding_id')->references('id')->on('prelandings')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('campaign_prelanding');
        Schema::dropIfExists('campaign_landing');
        Schema::dropIfExists('campaigns');
    }
}
