<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHtmlFilesTable extends Migration
{
    public function up()
    {
        Schema::create('html_files', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('target_id')->unsigned()->index();
            $table->string('target_type')->index();

            $table->string('original_name');

            $table->string('path');
            $table->string('mimetype');
            $table->integer('size');

            $table->timestamps();

            $table->index(['target_id', 'target_type']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('html_files');
    }
}
