<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignClicksTable extends Migration
{
    public function up()
    {
        Schema::create('campaign_clicks', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('clicks_count')->unsigned()->default(0);

            $table->integer('campaign_id')->unsigned();
            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('cascade');

            $table->integer('timestamp')->unsigned();
        });
    }

    public function down()
    {
        Schema::dropIfExists('campaign_clicks');
    }
}
