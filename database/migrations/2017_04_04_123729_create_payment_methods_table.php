<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentMethodsTable extends Migration
{
    public function up()
    {
        Schema::create('payment_methods', function (Blueprint $table) {

            $table->increments('id');

            $table->string('name');
            $table->text('description');
            $table->integer('minimum_amount')->unsigned()->default(0);
            $table->integer('commission')->unsigned()->default(0);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('payment_methods');
    }
}
