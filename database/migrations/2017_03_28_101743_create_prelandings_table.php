<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrelandingsTable extends Migration
{
    public function up()
    {
        Schema::create('prelandings', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('url');

            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

            $table->timestamps();
        });

    }

    public function down()
    {
        Schema::dropIfExists('prelandings');
    }
}
