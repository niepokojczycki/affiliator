<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get( '/',  ['as' => 'welcome',      'uses' => 'WelcomeController@show']);

// Login routes
Route::get( 'login',  ['as' => 'login',      'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login',  ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
Route::post('logout', ['as' => 'logout',     'uses' => 'Auth\LoginController@logout']);

// Registration Routes
Route::get( 'register',  ['as' => 'register',      'uses' => 'Auth\RegisterController@showRegistrationForm']);
Route::post('register',  ['as' => 'register.post', 'uses' => 'Auth\RegisterController@register']);


Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function ()
{
    Route::group(['prefix' => 'facebook', 'namespace' => 'Facebook'], function ()
    {
        Route::get('login',    ['as' => 'auth.facebook.login',    'uses' => 'AuthController@redirectToProvider']);
        Route::get('callback', ['as' => 'auth.facebook.callback', 'uses' => 'AuthController@handleProviderCallback']);
    });
});

Route::group(['middleware' => 'auth'], function() {
    Route::resource('products', 'ProductsController');

    Route::group(['prefix' => 'prelandings'], function() {
        Route::get('/{prelanding}/preview', ['as' => 'prelandings.preview', 'uses' => 'PrelandingsController@preview']);
    });

    Route::group(['prefix' => 'landings'], function() {
        Route::get('/{landing}/preview', ['as' => 'landings.preview', 'uses' => 'LandingsController@preview']);
    });

    Route::group(['prefix' => 'campaings'], function() {
        Route::post('/store',            ['as' => 'campaigns.store',  'uses' => 'CampaignsController@store']);
        Route::put( '{campaign}/update', ['as' => 'campaigns.update', 'uses' => 'CampaignsController@update']);
    });

    Route::get('dashboard',           ['as' => 'dashboard.show',     'uses' => 'DashboardController@show']);
    Route::get('dashboard/data',      ['as' => 'dashboard.get.data', 'uses' => 'DashboardController@getAjaxForMainChart']);

    Route::get('statistics',          ['as' => 'statistics.show',     'uses' => 'StatisticsController@show']);
    Route::get('statistics/download', ['as' => 'statistics.download', 'uses' => 'StatisticsController@download']);

    Route::group(['prefix' => 'payouts'], function() {
        Route::get( '/',               ['as' => 'payouts.index', 'uses' => 'PayoutRequestsController@index']);
        Route::post('create',          ['as' => 'payout.store',  'uses' => 'PayoutRequestsController@store']);
        Route::put( '{payout}/cancel', ['as' => 'payout.cancel', 'uses' => 'PayoutRequestsController@cancel']);
    });

    Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'namespace' => 'Admin'], function() {
        Route::get( '/', ['as' => 'admin', 'uses' => 'AdminController@index']);

        Route::resource('users',       'UsersController',       ['as' => 'admin']);
        Route::resource('products',    'ProductsController',    ['as' => 'admin']);
        Route::resource('landings',    'LandingsController',    ['as' => 'admin']);
        Route::resource('countries',   'CountriesController',   ['as' => 'admin']);
        Route::resource('campaigns',   'CampaignsController',   ['as' => 'admin']);
        Route::resource('prelandings', 'PrelandingsController', ['as' => 'admin']);
        Route::resource('prelandings', 'PrelandingsController', ['as' => 'admin']);
    });
});

Route::get('tracking/{uuid}',                   ['as' => 'campaigns.render.prelanding', 'uses' => 'CampaignsRenderController@renderPrelanding']);
Route::get('tracking/{uuid}/landing/{landing}', ['as' => 'campaigns.render.landing',    'uses' => 'CampaignsRenderController@renderLanding']);
Route::get('iframe/{uuid}',                     ['as' => 'campaigns.iframe.render',     'uses' => 'CampaignsRenderController@renderIframe']);
Route::get('details',                           ['as' => 'campaigns.details',           'uses' => 'CampaignsRenderController@getDetails']);