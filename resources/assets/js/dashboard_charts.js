$(document).ready(function () {
    createDonutCharts();
    createMainChart();
});

function createDonutCharts() {

    $(".conversionChart").each(function (index, element) {
        createDonutChart(index, element)
    });

    function createDonutChart(index, element) {
        let context = $(element).get(0).getContext("2d");
        let partialCount = $(element).data('partial-count');
        let totalCount = $(element).data('total-count');

        let pieData = [
            {
                value: partialCount,
                color: "#3F9F3F"
            },
            {
                value: (totalCount - partialCount),
                color: "#FFF"
            }
        ];

        new Chart(context).Doughnut(pieData, {percentageInnerCutout: 75});
    }
}


function createMainChart() {
    let chartCanvas = "#mainChart";
    let context = $(chartCanvas).get(0).getContext("2d");
    let dataUrl = $(chartCanvas).data('url');

    let options = {
        //Boolean - If we show the scale above the chart data
        responsive: true,

        scales: {
            yAxes: [{
                "id": 'Dollar',
                type: 'linear',
                position: 'left',
            }, {
                "id": "Numeric",
                type: 'linear',
                position: 'right'
            }]
        },

        //Interpolated JS string - can access value
        scaleLabel: function (valuePayload) {
            return Number(valuePayload.value).toFixed(2).replace('.', ',') + '$';
        },

        //Boolean - Whether the line is curved between points
        bezierCurve: false,

        //Boolean - Whether to show a dot for each point
        pointDot: true,

        //Number - Radius of each point dot in pixels
        pointDotRadius: 4,

        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth: 1,

        //Boolean - Whether to show a stroke for datasets
        datasetStroke: true,

        //Number - Pixel width of dataset stroke
        datasetStrokeWidth: 2,

        //Boolean - Whether to fill the dataset with a colour
        datasetFill: true,

        //Boolean - Whether to animate the chart
        animation: false,

        //Number - Number of animation steps
        animationSteps: 60,

        //String - Animation easing effect
        animationEasing: "easeOutQuart",

        //Function - Fires when the animation is complete
        onAnimationComplete: function () {
            //
        }
    };

    $.ajax({
        url: dataUrl,
        success: function (response) {
            function getData() {
                let fillColor = "rgba(252,233,79,0.5)";
                let strokeColor = "rgba(82,75,25,1)";
                let pointColor = "rgba(166,152,51,1)";
                let pointStrokeColor = "#fff";

                return {
                    labels: response.labels,
                    datasets: [
                        {
                            fillColor: fillColor,
                            strokeColor: strokeColor,
                            pointColor: pointColor,
                            pointStrokeColor: pointStrokeColor,
                            data: response.revenues,
                            label: "Revenue",
                            yAxisID: "Dollar"
                        },
                        {
                            fillColor: fillColor,
                            strokeColor: strokeColor,
                            pointColor: pointColor,
                            pointStrokeColor: pointStrokeColor,
                            data: response.clicks,
                            label: "Clicks",
                            yAxisID: "Numeric"
                        }
                    ]
                };
            }

            new Chart(context).Line(getData(), options);
        }
    });
}
