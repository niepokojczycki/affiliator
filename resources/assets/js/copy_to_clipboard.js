$(".copy-to-clipboard").on('click', function () {
    let contentToCopy = $(this)
        .closest(".input-group")
        .find(":input")
        .val();

    copyToClipboard(contentToCopy);

    swal('', 'Copied to clipboard', 'success')
        .catch(function (e) {
        })
});

function copyToClipboard(content) {
    let $temp = $("<input>");
    $("body").append($temp);
    $temp.val(content).select();
    document.execCommand("copy");
    $temp.remove();
}