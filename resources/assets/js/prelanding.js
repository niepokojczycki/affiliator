window.Laravel = {csrfToken: '{{ csrf_token() }}'};

require('./bootstrap');

$(document).ready(function () {
    $.ajax({
        url: details,
        type: 'get',

        success: function (response) {
            let landing_url = response.landing_url;
            $('.goto-landing').attr('action', landing_url);
        },

        error: function () {
        }
    });
});