let showMinimalValue = $('.minimal-value');
let amount = $("#amount");

$('#payment_method_id').on('change', function() {
    let selected = $(this).find('option:selected');
    let minimum = selected.data('minimum');
    let commission = selected.data('commission');
    changeMinimalValue();

    let totalValue = $("#total-value");
    totalValue.val(+amount.val() + +commission);

    function changeMinimalValue() {
        showMinimalValue.text(minimum);
    }
});

amount.on('change keyup paste', function() {
    let selected = $('#payment_method_id').find('option:selected');
    let minimum = selected.data('minimum');
    let commission = selected.data('commission');

    let totalValue = $("#total-value");
    totalValue.val(+amount.val() + +commission);
});
