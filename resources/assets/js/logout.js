$("#logout").on('click', function () {
    let route = $(this).data('href');
    axios.post(route)
        .then(function (response) {
            window.location = response.data.redirectTo
        })
        .catch();
})