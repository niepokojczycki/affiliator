let campaign = $(".campaign-form");

campaign.on("submit", function (e) {
    e.preventDefault();
    let url = this.action;
    let updating = $(this).find('input[name="_method"]').length;
    let method = updating ? 'put' : 'post';

    makeAjaxCall.call(this, url, method, updating);
});


function makeAjaxCall(url, method, updating) {
    $.ajax({
        url: url,
        type: method,
        dataType: 'json',
        data: $(this).serialize(),

        success: function (response) {
            let message = 'Campaign ' + (updating ? 'updated!' : 'created!');
            swal(message, '', 'success');

            fillFormFields(response);

            if (method =="post")
                changeFormMethod(response);
        },

        error: function (data) {
            let errors = data.responseJSON;
            let message = '';
            Object.keys(errors).map((e) => message += errors[e]);
            swal('Oops...', message, 'error')
        }
    });
}

function fillFormFields(response) {
    $("#target-url").val(response.trackingUrl);
    $("#iframe-form").val(response.trackingUrl);
}

function changeFormMethod(response) {
    campaign.attr('action', response.updateUrl);
    let methodChange = '<input type="hidden" name="_method" value="put">';
    campaign.append(methodChange);
}