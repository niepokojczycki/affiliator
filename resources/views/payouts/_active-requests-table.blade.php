<table class="table table-bordered">
    <thead>
    <tr>
        <th>ID</th>
        <th>date and time</th>
        <th>sum</th>
        <th>process</th>
        {{--<th>comment</th>--}}
        <th></th>
    </tr>
    </thead>
    @if ($payoutRequests->count())
        @foreach($payoutRequests as $payoutRequest)
            @include('payouts._payout-request-row')
        @endforeach
    @else
    @endif
</table>