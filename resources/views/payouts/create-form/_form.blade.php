{{ csrf_field() }}
<?php
// Todo: move this to some view presenter or something
$init_value = 100;

$firstPaymentMethod = $paymentMethods->first();

$minimumValue = $firstPaymentMethod->minimumAmount()->inDollars();
$commissionedValue = $init_value + $firstPaymentMethod->commission()->getInRawDollars();

?>
<div class="col-md-6">
    <h3>Withdrawal from the system</h3>

    <div class="row margin-5px margin-top-20px">
        <div class="col-md-6 text-align-right">
            <label for="amount">Amount</label>
        </div>
        <div class="col-md-6">
            <div class="input-group">
                <input name="amount" type="text" class="form-control" id="amount" value="{{ $init_value }}">
                <div class="input-group-addon">at least <span class="minimal-value">{{ $minimumValue }}</span></div>
            </div>
        </div>
    </div>

    <div class="row margin-5px ">
        <div class="col-md-6 text-align-right">
            <label for="payment_method_id">Payment method</label>
        </div>
        <div class="col-md-6">
            <select class="form-control" id="payment_method_id" name="payment_method_id">
                @foreach($paymentMethods as $paymentMethod)
                    <option value="{{ $paymentMethod->id }}"
                            data-minimum="{{ $paymentMethod->minimum_amount }}"
                            data-commission="{{ $paymentMethod->commission()->getInRawDollars() }}">{{ $paymentMethod->name}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="row margin-5px">
        <div class="col-md-6 text-align-right">
            <label for="total-value">Amount with commission</label>
        </div>
        <div class="col-md-2">
            <input type="text" class="form-control" id="total-value" value="{{ $commissionedValue }}">
        </div>
    </div>

    <div class="row margin-5px">
        <div class="col-md-12 text-align-right">
            <button type="submit" class="btn btn-lg btn-primary btn-block center-block">SEND</button>
        </div>
    </div>
</div>
