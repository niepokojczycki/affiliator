@extends('layouts.main')

@section('content')

    <style type="text/css">
        .requests-history-container {
            margin-top: 10px;
        }

        .requests-history-h2 {
            margin-bottom: 20px;
            margin-left: 10px;
        }

        .margin-5px {
            margin: 5px 0;
        }

        .padding-30px {
            padding: 30px 0;
        }

        .margin-top-20px {
            margin-top: 20px;
        }

        .text-talign-right {
            text-align: right;
        }
    </style>
    <div class="dashboard-top-inside">
        <div class="container">
            <h1><i class="fa fa-money" aria-hidden="true"></i> Payout</h1>
        </div>
    </div>

    <div class="container">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        <div class="alert alert-info" role="alert">
            <h3>Your Balance: {{ $user->account()->balance()->inDollars() }}.</h3>
        </div>
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Funds will be transferred within 7 days from the date of the payment order
        </div>

        <div class="row">
            <div class="withdrawal padding-30px">
                <form action="{{ route('payout.store') }}" method="POST">
                    @include('payouts.create-form._form')
                </form>

                <div class="col-md-6">
                    <h3>Withdrawal requests</h3>
                    @include('payouts._active-requests-table')
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="white-box requests-history-container">
            <h2 class="requests-history-h2">History</h2>
            @include('payouts._history-requests-table')

            <nav aria-label="Page navigation">
                {{ $payoutsHistory->render() }}
            </nav>
        </div>
    </div>


@endsection

@section('scripts')
    @parent
    <script src="{{ mix('js/payout.js')}}"></script>
@endsection
