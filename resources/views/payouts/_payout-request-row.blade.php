<style type="text/css">
    .awaiting-request-row {
        background: #fff;
    }
</style>

<tr class="awaiting-request-row">
    <td>{{ $payoutRequest->id }}</td>
    <td>{{ $payoutRequest->created_at->format('d F, Y H:i')}}</td>
    <td>{{ $payoutRequest->amount()->inDollars() }}</td>
    <td>{{ $payoutRequest->paymentMethod->name }}</td>

    <td>
        <form action="{{ route('payout.cancel', compact('payoutRequest')) }}" method="post">
            {{ method_field('PUT') }}
            {{ csrf_field() }}

            <button type="submit" class="btn btn-sm btn-warning">Cancel</button>
        </form>
    </td>
</tr>
