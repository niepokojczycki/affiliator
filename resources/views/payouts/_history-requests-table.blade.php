<table class="table">
    <thead>
    <tr>
        <th>Date and Time</th>
        <th>Sum</th>
        <th>Comment</th>
    </tr>
    </thead>
    <tbody>
    @if($payoutsHistory->count())
        @foreach($payoutsHistory as $historyItem)
            @include('payouts._payout-history-row')
        @endforeach
    @else
        <tr class="centered">
            <td>No history of payouts yet!</td>
        </tr>
    @endif
    </tbody>
</table>
