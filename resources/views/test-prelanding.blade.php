<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TEST PRELANDING</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
</head>
<body>


<div class="dashboard-top-inside">
    <div class="container">
        <h1><i class="fa fa-bar-chart" aria-hidden="true"></i> Test Prelanding</h1>
    </div>
</div>

<div id="content-inside">
    <div class="container centered">

        <h2 class="prelanding-title">Prelanding title!</h2>
        Proof that this prelanding is actually rendering. Click on the button below to be forwarded into landing.

        I belong to campaign with uuid of <span class="campaign-uuid"></span>

        <form action="#" class="goto-landing">
            <input type="submit" value="Go to landing!" />
        </form>
    </div>
</div>

</body>
</html>
