<div class="form-group">
    <label for="{{ $id }}">{{ $name }}</label>
    <div class="input-group">
        <input type="text" class="form-control" id="{{ $id }}" name="{{ $id }}"
               value="{{ $value }}">
        <span class="input-group-btn">
            <button class="btn btn-success copy-to-clipboard" type="button">
                Copy <i class="fa fa-clipboard" aria-hidden="true"></i>
            </button>
        </span>
    </div>
</div>