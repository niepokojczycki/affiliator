<tr class="linkable-row" data-href="{{ route('products.show', compact('product')) }}">
    <th scope="row">{{ $product->id}}</th>
    <td>{{ $product->name }}</td>
    <td>{{ $product->payout()->inDollars() }}</td>
    <td>{{ $product->country->name }}</td>
    <td>{{ $product->present()->categoriesCount() }}</td>
    <td>{{ $product->created_at }}</td>
</tr>