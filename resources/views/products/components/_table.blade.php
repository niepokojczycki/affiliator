<table class="table">
    <caption></caption>
    <thead>
    <tr>
        <th>#ID</th>
        <th>Name</th>
        <th>Payout</th>
        <th>Country</th>
        <th>Category</th>
        <th>Created at</th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
        @if(isset($adminPanel) and $adminPanel === true)
            @include('admin.products.components._table_row')
        @else
            @include('products.components._table_row')
        @endif
    @endforeach

    </tbody>
</table>