@extends('layouts.main')

@section('content')

    <style type="text/css">
        .margin-bottom-20 {
            margin-bottom: 20px;
        }

    </style>
    <div class="dashboard-top-inside">
        <div class="container">
            <h1><i class="fa fa-list" aria-hidden="true"></i> {{ $product->name }}</h1>
        </div>
    </div>

    <div id="content-inside">
        <div class="container">
            <div class="col-md-8">

                <form action="{{ $formAction }}" method="post" class="campaign-form">
                    {{ csrf_field() }}

                    @if($campaign->exists)
                        {{ method_field('put') }}
                    @endif

                    <input type="hidden" value="{{ $product->id }}" name="product_id">

                    @include('products.components._copyable', [
                    'id' => 'target-url',
                    'name' => 'Target URL',
                    'value' => $campaign->present()->trackingUrl()])
                    @include('products.components._copyable', [
                    'id' => 'iframe-form',
                    'name' => 'iFrame Form',
                    'value' => $campaign->present()->iframeForm()])

                    <div class="row margin-bottom-20">
                        <div class="col-md-6">
                            @include('products._landings')
                        </div>
                        <div class="col-md-6">
                            @include('products._prelandings')
                        </div>
                    </div>

                    <div class="form-group">
                        {{--<strong>Comebacker</strong>--}}
                        {{--<div class="radio">--}}
                        {{--<label>--}}
                        {{--<input type="radio" name="optionsRadios" id="optionsRadios" value="option1" checked>--}}
                        {{--Default (system settings used)--}}
                        {{--</label>--}}
                        {{--<div class="radio">--}}
                        {{--<label>--}}
                        {{--<input type="radio" name="optionsRadios" id="optionsRadios" value="option2">--}}
                        {{--Disabled--}}
                        {{--</label>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="form-group">
                            <label for="postback_leads">Postback URL for leads</label>
                            <input type="text" class="form-control" id="postback_leads" name="postback_leads"
                                   value="{{$campaign->postback_leads}}">
                        </div>
                        <div class="form-group">
                            <label for="postback_holds">Pastback URL for holds</label>
                            <input type="text" class="form-control" id="postback_holds" name="postback_holds"
                                   value="{{$campaign->postback_holds}}">
                        </div>
                        <div class="form-group">
                            <label for="postback_rejects">Postback URL for rejects</label>
                            <input type="text" class="form-control" id="postback_rejects" name="postback_rejects"
                                   value="{{$campaign->postback_rejects}}">
                        </div>
                        <div class="form-group">
                            <label for="pixel_id">Pixel ID</label>
                            <input type="text" class="form-control" id="pixel_id" name="pixel_id"
                                   value="{{$campaign->pixel_id}}">
                        </div>
                        <div class="form-group">
                            <label for="retarget_code">Retarget code</label>
                            <input type="text" class="form-control" id="retarget_code" name="retarget_code"
                                   value="{{$campaign->retarget_code}}">
                        </div>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>

            <div class="col-md-4">
                <div id="poster">
                    <img src="{!! $product->present()->croppaImagePath(300,300) !!}"
                         alt="{{ $product->present()->imageName() }}">
                    <p>
                        <strong>{{ $product->name }}</strong>
                    </p>
                    <div id="poster-bottom">
                        <h3>Description</h3>
                        {{ $product->description }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ mix('js/copy_to_clipboard.js')  }}"></script>
    <script src="{{ mix('js/campaign_form.js')  }}"></script>
@endsection