@extends('layouts.main')

@section('content')

    @include('products.filters.filters')

    <div class="col-xs-12">
        @include('products.components._table')
    </div>

    <div class="pagination">
        {{ $products->render() }}
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function ($) {
            $(".linkable-row").click(function () {
                window.location = $(this).data("href");
            });
        });
    </script>
@endsection
