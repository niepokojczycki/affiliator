<div class="row single-filter">
    <div class="col-md-2">
        <label for="{{ $filterName }}">{{ $title }}</label>
    </div>
    <div class="col-md-6">
        {{ $input }}
    </div>
</div>
