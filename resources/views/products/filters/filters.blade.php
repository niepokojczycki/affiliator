<style type="text/css">
    .filters-row {
        margin-left: 10px;
        margin-top: 3rem;
    }

    .single-filter {
        margin: 5px 0;
    }

    .products-h2 {
        margin-top: 0;
        padding-top: 0;
    }

</style>
<div class="row filters-row">
    <form action="{{ route('products.index') }}" method="get" id="filters-form">
        <div class="col-md-6">
            <h2 class="products-h2">Product List</h2>
            <p>You can choose a program that will promote</p>
            <p>Total available : <strong>{{ $products->total() }} products</strong></p>
        </div>
        <div class="col-md-6">
            @component('products.filters.filter')
                @slot('filterName')
                    category
                @endslot
                @slot('title')
                    Category
                @endslot
                @slot('input')
                    <select class="form-control" id="category" name="category">
                        <option {{ request()->get('category') ? '' : 'selected' }} value>All categories</option>
                        @foreach($categories as $category)
                            <option {{request()->get('category') == $category->id ? 'selected' : ''}}
                                    value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach

                    </select>
                @endslot
            @endcomponent

            @component('products.filters.filter')
                @slot('filterName')
                    country
                @endslot
                @slot('title')
                    Country
                @endslot
                @slot('input')
                    <select class="form-control" id="country" name="country">
                        <option {{ request()->get('country') ? '' : 'selected' }} value>All countries</option>
                        @foreach($countries as $country)
                            <option {{request()->get('country') == $country->id ? 'selected' : ''}}
                                    value="{{ $country->id }}">{{ $country->name }}</option>
                        @endforeach
                    </select>
                @endslot
            @endcomponent

            @component('products.filters.filter')
                @slot('filterName')
                    name
                @endslot
                @slot('title')
                    Name
                @endslot
                @slot('input')
                    <input type="text" class="form-control" id="name" name="name" placeholder="name"
                           value="{{ request()->get('name') }}">
                @endslot
            @endcomponent
        </div>

        <div class="row centered">
            <div class="col-md-8">
                <button type="submit" class="btn btn-block btn-success">Choose</button>
            </div>
        </div>
    </form>
</div>