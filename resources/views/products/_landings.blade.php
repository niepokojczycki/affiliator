<strong>Landings</strong>

@foreach($product->landings as $landing)
    <div class="checkbox">
        <label>
            <input type="checkbox" value="{{ $landing->id }}" name="landings[]"
            {{ $campaign->present()->landingCheckbox($landing->id) }}>
            {{ $landing->name }}
            <a href="{{ route('landings.preview', compact('landing')) }}" target="_blank">
                <i class="fa fa-globe" aria-hidden="true"></i>
            </a>
        </label>
    </div>
@endforeach

<a href="#"><i class="fa fa-chevron-down" aria-hidden="true"></i> Show All</a>