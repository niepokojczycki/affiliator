<strong>Prelandings</strong>

@foreach($product->prelandings as $prelanding)
    <div class="checkbox">
        <label>
            <input type="checkbox" value="{{ $prelanding->id }}" name="prelandings[]"
                    {{ $campaign->present()->prelandingCheckbox($prelanding->id) }}>
            {{ $prelanding->name }}
            <a href="{{ route('prelandings.preview', compact('prelanding')) }}" target="_blank">
                <i class="fa fa-globe" aria-hidden="true"></i>
            </a>
        </label>
    </div>
@endforeach

<a href="#"><i class="fa fa-chevron-down" aria-hidden="true"></i> Show All</a>