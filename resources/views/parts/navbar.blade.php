<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name') }}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                @if (Auth::check())
                    <li><a href="{{ route('dashboard.show') }}"><i class="fa fa-tachometer" aria-hidden="true"></i>&nbsp;Dashboard</a>
                    </li>
                    <li><a href="{{ route('products.index') }}"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Offers</a>
                    </li>
                    <li><a href="{{ route('statistics.show') }}"><i class="fa fa-money" aria-hidden="true"></i>&nbsp;Statistics</a>
                    </li>
                    <li><a href="{{  route('payouts.index') }}"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;Payout</a>
                    </li>

                    @can('admin', Auth::user())
                        <li><a href="{{ route('admin') }}">Admin</a></li>
                    @endcan
                @endif
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())

                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                @else
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a id="logout" href="#" data-href="{{ route('logout') }}">
                                <i class="fa fa-sign-out" aria-hidden="true"></i>Logout
                            </a>
                        </li>
                        <li><a href="#">Edit profile data</a></li>

                    </ul>
                @endif
            </ul>
        </div>
    </div>
</nav>