<div>
    <label for="{{ $variableName }}">{{ $title }}</label>
    <textarea rows="{{$rows ?? '5'}}" class="form-control {{ $errors->has($variableName) ? 'has-error' : '' }}"
              id="{{ $variableName }}" name="{{ $variableName }}"></textarea>

    @if($errors->has($variableName))
        <span class="help-block">{{ $errors->first($variableName) }}</span>
    @endif
</div>

