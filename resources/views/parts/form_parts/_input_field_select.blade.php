<div>
    <label for="{{ $variableName }}">{{ $title }}</label>
    <select {{ (isset($multiple) and $multiple === true) ? "multiple" : '' }} id="{{ $variableName }}" name="{{ $variableName }}"
            class="{{ $errors->has($variableName) ? 'has-error' : '' }}">
        @foreach($items as $item)
            <option {{ in_array($item->id, $selected) ? 'selected' : ''}}
                    value="{{ $item->id }}">{{ $item->name }}</option>
        @endforeach
    </select>
    @if($errors->has($variableName))
        <span class="help-block">{{ $errors->first($variableName) }}</span>
    @endif
</div>