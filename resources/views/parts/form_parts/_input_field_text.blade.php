<div>
    <label for="{{ $variableName }}">{{ $title }}</label>
    <input class="form-control {{ $errors->has($variableName) ? 'has-error' : '' }}" id="{{ $variableName }}"
           name="{{ $variableName }}" value="{{ $value }}">
    @if($errors->has($variableName))
        <span class="help-block">{{ $errors->first($variableName) }}</span>
    @endif
</div>