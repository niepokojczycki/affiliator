<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }}</title>

    <link rel="stylesheet" type="text/css" href="{{ mix('css/app.css')  }}">

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">



    @yield('header')
</head>
<body>
<div id="app">
    @yield('before_top')

    @yield('before_content')

    @yield('content')

    @yield('after_content')
</div>

@yield('before_scripts')

<script>
    window.Laravel = {csrfToken: '{{ csrf_token() }}'};
</script>

<script src="{{ mix('js/app.js')  }}"></script>

@yield('scripts')

@yield('after_scripts')
</body>
</html>
