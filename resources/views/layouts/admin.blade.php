@extends('layouts.app')

@section('before_content')
@append

@section('content')
    @include('parts.navbar')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('admin._menu')
            </div>

            <div class="col-md-9">
               @yield('admin-content')
            </div>
        </div>
    </div>
@append

@section('after_content')
    @include('parts.footer')
@append