@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="container-fluid">
                                <div class="pull-left">
                                    Register
                                </div>
                                <div class="pull-right">
                                    <a href="{{ route('auth.facebook.login') }}" class="btn btn-info">Register with
                                        Facebook</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{!! $error !!}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group">
                            <form action="{{ route('register.post') }}" method="post">
                                {{ csrf_field() }}
                                
                                <div>
                                    <label for="email">Email</label>
                                    <input class="form-control" id="email" name="email" value="{{ old('email') }}">
                                </div>

                                <div>
                                    <label for="password">password</label>
                                    <input class="form-control" id="password" name="password" type="password">
                                </div>

                                <div>
                                    <label for="password_confirmation">confirm password</label>
                                    <input class="form-control" id="password_confirmation" name="password_confirmation" type="password">
                                </div>

                                <div>
                                    <label for="first_name">First name</label>
                                    <input class="form-control" id="first_name" name="first_name" value="{{ old('first_name') }}">
                                </div>
                                
                                <div>
                                    <label for="last_name">Last name</label>
                                    <input class="form-control" id="last_name" name="last_name" value="{{ old('last_name') }}">
                                </div>

                                <div>
                                    <label for="country_id">country</label>
                                    <select class="form-control" id="country_id" name="country_id">
                                        @foreach($countries as $id => $name)
                                            <option value="{{ $id }}">{{ $name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div>
                                    <label for="city">city</label>
                                    <input class="form-control" id="city" name="city" value="{{ old('city') }}">
                                </div>

                                <div>
                                    <label for="state">state</label>
                                    <input class="form-control" id="state" name="state" value="{{ old('state') }}">
                                </div>

                                <div>
                                    <label for="postal">postal</label>
                                    <input class="form-control" id="postal" name="postal" value="{{ old('postal') }}">
                                </div>

                                <div>
                                    <label for="address">address</label>
                                    <input class="form-control" id="address" name="address"
                                           value="{{ old('address') }}">
                                </div>

                                <div>
                                    <label for="phone">phone</label>
                                    <input class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
                                </div>

                                <hr>

                                <p class="control">
                                    <button class="btn btn-primary" type="submit">Register</button>
                                </p>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
