@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="container-fluid">
                                <div class="pull-left">
                                    Login
                                </div>
                                <div class="pull-right">
                                    <a href="{{ route('auth.facebook.login') }}" class="btn btn-info">Login with
                                        Facebook</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{!! $error !!}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group">
                            <form action="{{ route('login.post') }}" method="post">
                                {{ csrf_field() }}

                                <div>
                                    <label for="email">Email</label>
                                    <input class="form-control" id="email" name="email" value="{{ old('email') }}">
                                </div>

                                <div>
                                    <label for="password">password</label>
                                    <input class="form-control" id="password" name="password" type="password">
                                </div>

                                <hr>

                                <p class="control">
                                    <button class="btn btn-primary" type="submit">Login</button>
                                </p>

                            </form>
                            <a class="btn btn-link" href="#">
                                Forgot Your Password?
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
