@extends('layouts.admin')

@section('admin-content')

    <div>
        <h2>Prelanding name: <span class="strong">{{ $prelanding->name }}</span></h2>
        <h3><a href="{{ route('admin.prelandings.edit', compact('prelanding')) }}">Edit</a></h3>

        <h4><a href="{{ route('admin.products.show', ['product' => $prelanding->product]) }}">Go to product</a></h4>
        <h5><a href="{{ route('prelandings.preview', compact('prelanding')) }}">Preview</a></h5>
    </div>
@endsection
