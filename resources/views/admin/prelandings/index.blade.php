@extends('layouts.admin')

@section('admin-content')

    <div class="row filters-row">
        <form action="{{ route('admin.prelandings.index') }}" method="get" id="filters-form">
            <div class="col-md-12">
                <h2 class="products-h2">Prelandings List</h2>
                <strong>{{ $prelandings->total() }} prelandings</strong>
            </div>

            <div class="row centered">
                <div class="col-md-4">
                    <a href="{{ route('admin.prelandings.create') }}" class="btn btn-primary btn-block">Add new</a>
                </div>
            </div>
        </form>
    </div>

    @include('admin.prelandings.components._table')

    {{ $prelandings->render() }}
@endsection