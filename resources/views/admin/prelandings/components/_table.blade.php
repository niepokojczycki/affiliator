<table class="table">
    <caption></caption>
    <thead>
    <tr>
        <th>#ID</th>
        <th>Name</th>
        <th>Product name</th>
        <th>URL</th>
        <th>Created at</th>
    </tr>
    </thead>
    <tbody>
    @foreach($prelandings as $prelanding)
        <tr>
            <td>{{ $prelanding->id }}</td>
            <td><a href="{{ route('admin.prelandings.show', compact('prelanding')) }}">{{ $prelanding->name }}</a></td>
            <td>
                <a href="{{ route('admin.products.show', ['product' => $prelanding->product]) }}">{{ $prelanding->product->name }}</a>
            </td>
            <td>
                <a href="{{ $prelanding->url }}">{{ $prelanding->url }}</a>
            </td>
            <td>{{ $prelanding->created_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>