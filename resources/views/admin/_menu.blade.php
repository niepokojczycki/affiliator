<style type="text/css">
    .bold {
        font-weight: bold;
    }
</style>
<div>
    <ul>
        @foreach($adminPaths as $name => $route)
            <li>
                <a href="{{ $route }}">
                    <span class="{{ $route == request()->url() ? 'bold' : '' }}">{{ $name }}</span>
                </a>
            </li>
        @endforeach
    </ul>

</div>