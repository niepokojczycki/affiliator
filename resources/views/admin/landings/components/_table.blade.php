<table class="table">
    <caption></caption>
    <thead>
    <tr>
        <th>#ID</th>
        <th>Name</th>
        <th>Product name</th>
        <th>URL</th>
        <th>Created at</th>
    </tr>
    </thead>
    <tbody>
    @foreach($landings as $landing)
        <tr>
            <td>{{ $landing->id }}</td>
            <td><a href="{{ route('admin.landings.show', compact('landing')) }}">{{ $landing->name }}</a></td>
            <td>
                <a href="{{ route('admin.products.show', ['product' => $landing->product]) }}">{{ $landing->product->name }}</a>
            </td>
            <td>
                <a href="{{ $landing->url }}">{{ $landing->url }}</a>
            </td>
            <td>{{ $landing->created_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>