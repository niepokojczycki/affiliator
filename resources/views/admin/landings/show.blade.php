@extends('layouts.admin')

@section('admin-content')

    <div>
        <h2>Landing name: <span class="strong">{{ $landing->name }}</span></h2>

        <h3><a href="{{ route('admin.landings.edit', compact('landing')) }}">Edit</a></h3>
        <h4><a href="{{ route('admin.products.show', ['product' => $landing->product]) }}">Go to product</a></h4>
        <h5><a href="{{ route('landings.preview', compact('landing')) }}">Preview</a></h5>
    </div>
@endsection