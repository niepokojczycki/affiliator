@extends('layouts.admin')

@section('admin-content')

    <div class="row filters-row">
        <form action="{{ route('admin.landings.index') }}" method="get" id="filters-form">
            <div class="col-md-12">
                <h2 class="products-h2">Landings List</h2>
                <strong>{{ $landings->total() }} landings</strong>
            </div>

            <div class="row centered">
                <div class="col-md-4">
                    <a href="{{ route('admin.landings.create') }}" class="btn btn-primary btn-block">Add new</a>
                </div>
            </div>
        </form>
    </div>

    @include('admin.landings.components._table')

    {{ $landings->render() }}
@endsection