@extends('layouts.admin')

@section('admin-content')

    @include('admin.products.components._form')
@endsection

@section('scripts')
    @parent

    <script type="text/javascript">
        $('[name="categories[]"]').selectize({
            plugins: ['remove_button'],
            mode: 'multi',
            maxItems: 99
        });

        $('[name="country"]').selectize({
            plugins: ['remove_button'],
            mode: 'multi',
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                let reader = new FileReader();

                reader.onload = function (e) {
                    let preview = $('#image-preview');
                    preview.attr('src', e.target.result);
                    preview.removeClass('hidden');
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#image").change(function(){
            readURL(this);
        });
    </script>
@endsection