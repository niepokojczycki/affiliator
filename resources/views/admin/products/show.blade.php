@extends('layouts.admin')

@section('admin-content')

    <div class="container">
        <h2>Product: {{ $product->name }}</h2>

        <h4>Payout: {{$product->payout()->inDollars() }}</h4>

        <h5>Categories: {{$product->present()->categoriesCount() }}</h5>

        <h5>Description: {{$product->description}}</h5>

        <a href="{{ route('admin.products.edit', compact('product')) }}">Edit</a>

        <form class="product-delete" action="{{ route('admin.products.destroy', compact('product')) }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('delete') }}

            <button type="submit">Delete</button>
        </form>
        @if($product->landings->count())
            <h3>Landings</h3>
            <ul>
                @foreach($product->landings as $landing)
                    <li>
                        {{ $landing->name }} <a href="{{ route('landings.preview', compact('landing')) }}">preview</a>
                        <a class="delete" href="#"
                           data-href="{{ route('admin.landings.destroy', compact('landing')) }}">Delete</a>
                    </li>
                @endforeach
            </ul>
        @else
            <br>
            <span>Product doesn't have any landings. Without landings product won't be visible for users.
                <a href="{{ route('admin.landings.create') }}">Add them</a></span>
        @endif

        @if($product->prelandings->count())
            <h3>Preandings</h3>
            <ul>
                @foreach($product->prelandings as $prelanding)
                    <li>
                        {{ $prelanding->name }} <a href="{{ route('prelandings.preview', compact('prelanding')) }}">preview</a>
                        <a class="delete" href="#"
                           data-href="{{ route('admin.prelandings.destroy', compact('prelanding')) }}">Delete</a>
                    </li>
                @endforeach
            </ul>
        @else
            <br>
            <span>Product doesn't have any prelandings.<a
                        href="{{ route('admin.landings.create') }}">Add them</a></span>
        @endif
    </div>
@endsection

@section('scripts')
    @parent

    <script type="text/javascript">
        $(".delete").on('click', function () {
            if (confirm("Are you sure?")) {
                $.ajax({
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    method: 'delete',
                    url: $(this).data('href'),
                    success: $(this).parent().remove()
                });
            }
        });

        $(".product-delete").on('submit', function (e) {
            if (confirm('Are you sure?'))
                return true;

            e.preventDefault();
        })
    </script>
@endsection