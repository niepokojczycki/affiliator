<tr class="linkable-row">
    <th scope="row">{{ $product->id}}</th>
    <td><a href="{{ route('admin.products.show', compact('product')) }}">{{ $product->name }}</a></td>
    <td>{{ $product->payout()->inDollars() }}</td>
    <td>{{ $product->country->name }}</td>
    <td>{{ $product->present()->categoriesCount() }}</td>
    <td>{{ $product->created_at }}</td>
</tr>