<?php

$update = (isset($product) and $product->exists);

$formAction = $update
    ? route('admin.products.update', compact('product'))
    : route('admin.products.store')
?>

<style type="text/css">
    .help-block {
        color: #ff5722;
    }
</style>


<div class="form-group">
    <form action="{{ $formAction }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        @if($update)
            {{ method_field('PUT') }}
        @endif

        @include('parts.form_parts._input_field_text',     ['title' => "Name",                'variableName' => "name",         'value' => $update ? $product->name : old('name')])
        @include('parts.form_parts._input_field_textarea', ['title' => "Description",         'variableName' => "description",  'value' => $update ? $product->description : old('description')])
        @include('parts.form_parts._input_field_text',     ['title' => "Payout (in dollars)", 'variableName' => "payout",       'value' => $update ? $product->payout()->getInRawDollars() : old('payout')])
        @include('parts.form_parts._input_field_select',   ['title' => "Categories",          'variableName' => "categories[]", 'selected' => $update ? $product->categories->pluck('id')->all() : [old('categories')], 'items' => $categories, 'multiple' => true])
        @include('parts.form_parts._input_field_select',   ['title' => "Country",             'variableName' => "country",      'selected' => $update ? [$product->country_id] : [old('country')],  'items' => $countries])

        <input type="file" name="image" id="image">

        <img class="hidden" id="image-preview" src="#" alt="Image preview" height="100" width="100"/>

        <button type="submit" class="btn btn-success">Save</button>
    </form>
</div>