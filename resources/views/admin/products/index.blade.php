@extends('layouts.admin')

@section('admin-content')

    <div class="row filters-row">
        <form action="{{ route('admin.products.index') }}" method="get" id="filters-form">
            <div class="col-md-12">
                <h2 class="products-h2">Product List</h2>
                <strong>{{ $products->total() }} products</strong>
            </div>

            <div class="row centered">
                <div class="col-md-4">
                    <a href="{{ route('admin.products.create') }}" class="btn btn-primary btn-block">Add new</a>
                </div>
            </div>
        </form>
    </div>

    @include('products.components._table')

    {{ $products->render() }}
@endsection