<style type="text/css">
    .help-block {
        color: #ff5722;
    }
</style>

<?php
$update = $landingable->exists;
$input_name = $update ? $landingable->name : old('name');
$input_url = $update ? $landingable->url : old('url');
$input_selected = $update ? [$landingable->product_id] : [old('product')];
?>

<div class="form-group">
    <form action="{{ $formAction }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        @if($update)
            {{ method_field('PUT') }}
        @endif

        @include('parts.form_parts._input_field_text',   ['title' => "Name",    'variableName' => "name",    "value" => $input_name])
        @include('parts.form_parts._input_field_text',   ['title' => "Url",     'variableName' => "url",     "value" => $input_url])
        @include('parts.form_parts._input_field_select', ['title' => "Product", 'variableName' => "product", "items" => $products, 'selected' => $input_selected])

        <br>
        <button type="submit" class="btn btn-success">Save</button>
    </form>
</div>