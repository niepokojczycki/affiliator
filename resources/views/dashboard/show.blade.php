@extends('layouts.main')

@section('content')
    <div class="container">

        <div class="dashboard-top">
            <div class="row">
                @foreach($topElements as $element)
                    @include('dashboard.components.top-element')
                @endforeach
            </div>
        </div>
        <div id="dashboard-bottom">

            <div class="row">
                <div class="col-md-8">
                    <canvas id="mainChart" data-url="{{ route('dashboard.get.data') }}"></canvas>
                </div>

                <div class="col-md-4">
                    @foreach($conversionRatios as $conversionRatio)
                        @include('dashboard.components.conversion-ratio')
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script src="http://www.chartjs.org/assets/Chart.js"></script>
    <script src="{{ mix('js/dashboard_charts.js') }}"></script>

@endsection