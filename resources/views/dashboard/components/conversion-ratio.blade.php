<div class="white-box right-box">
    <div class="chart-left col-md-12 pull-left">
        <h3>{{ $conversionRatio->getTitle() }}</h3>
        <canvas class="conversionChart"
                data-partial-count="{{ $conversionRatio->getPartialCount() }}"
                data-total-count="{{ $conversionRatio->getTotalCount() }}"></canvas>
    </div>
    <div class="clearfix"></div>
</div>
