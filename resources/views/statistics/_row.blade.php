<tr>
    <td>{{ $campaign->product->name }}</td>
    <td>{{ $campaign->statistics()->clicks() }}</td>
    <td>{{ $campaign->statistics()->leads() }}</td>
    <td>{{ $campaign->statistics()->sales() }}</td>
    <td>{{ $campaign->statistics()->awaiting() }}</td>
    <td>{{ $campaign->statistics()->rejects() }}</td>
    <td>{{ $campaign->statistics()->epc() }}</td>
    <td>{{ $campaign->statistics()->leadsRevenue()->inDollars() }}</td>
    <td>{{ $campaign->statistics()->salesRevenue()->inDollars() }}</td>
    <td>{{ $campaign->statistics()->holdsRevenue()->inDollars() }}</td>
    <td>{{ $campaign->statistics()->totalRevenue()->inDollars() }}</td>
</tr>
                    