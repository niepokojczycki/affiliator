@extends('layouts.main')

@section('content')
    <div class="dashboard-top-inside">
        <div class="container">
            <h1><i class="fa fa-bar-chart" aria-hidden="true"></i> Statistics</h1>
        </div>
    </div>

    <div id="content-inside">
        <div class="container">
            <form class="form-inline" style="margin-left:20px;">

                <div class="row" style="padding:20px 0;">
                    <div class="col-md-12">

                        <div class="form-group" style="margin-right:15px;">
                            <label for="">For Period</label>
                            <select class="form-control">
                                <option>14 Jan 17 - 20 Jan</option>
                            </select>
                        </div>

                        <div class="form-group" style="margin-right:15px;">
                            <button type="button" class="btn btn-success btn-sm">
                                SHOW
                            </button>
                            <a href="{{ route('statistics.download') }}" class="btn btn-warning btn-sm">
                                <i class="fa fa-download" aria-hidden="true"></i> CSV
                            </a>
                        </div>
                    </div>
                </div>
            </form>
            <div class="white-box" style="margin-top:10px;">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Offer</th>
                        <th>Clicks</th>
                        <th>Leads</th>
                        <th>Sales</th>
                        <th>Awaiting</th>
                        <th>Rejected</th>
                        <th>EPC</th>
                        <th>Lead</th>
                        <th>Sales</th>
                        <th>Hold</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tr>
                        @if($campaigns->count())
                            @foreach($campaigns as $campaign)
                                @include('statistics._row')
                            @endforeach
                        @else
                            <td class="bg-success" style="text-align:center;" colspan="11">Empty</td>
                        @endif
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection