<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 28.03.17
 * Time: 11:18
 */

namespace App\Entities;


use App\Support\Money;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class PayoutRequest extends Model implements Transformable
{
    const AWAITING = 0;
    const PAID = 1;
    const CANCELED = 2;

    public static $statuses = [
        self::AWAITING => 'Awaiting',
        self::PAID     => 'Paid',
        self::CANCELED => 'Canceled',
    ];

    use TransformableTrait;

    protected $fillable = ['status', 'amount'];

    public static function store(User $user, Money $amount, PaymentMethod $paymentMethod)
    {
        $totalWithdraw = $amount->copy()->add($paymentMethod->commission());
        $user->account()->withdraw($totalWithdraw);

        $payoutRequest = new self([
            'amount' => $amount->getAmount(),
            'status' => self::AWAITING,
        ]);

        $payoutRequest->user()->associate($user);
        $payoutRequest->paymentMethod()->associate($paymentMethod);
        $payoutRequest->save();

        return $payoutRequest;
    }

    public function paymentMethod(): BelongsTo
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function amount()
    {
        return new Money($this->amount);
    }

    public function cancel()
    {
        $this->update(['status' => self::CANCELED]);

        // we will return the commission costs if payout wasn't completed.
        $this->user->account()
            ->add($this->amount())
            ->add($this->paymentMethod->commission());

    }
}






