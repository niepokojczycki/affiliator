<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 28.03.17
 * Time: 11:18
 */

namespace App\Entities;


use App\Support\Money;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class PaymentMethod extends Model implements Transformable
{

    use TransformableTrait;

    protected $fillable = ['name', 'description', 'commission', 'minimum_amount'];

    public function minimumAmount()
    {
        return new Money($this->minimum_amount);
    }

    public function commission()
    {
        return new Money($this->commission);
    }
}






