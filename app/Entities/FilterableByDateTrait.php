<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 03.04.17
 * Time: 16:15
 */

namespace App\Entities;


use Carbon\Carbon;

trait FilterableByDateTrait
{
    public function isBetweenDates(Carbon $dateFrom = null, Carbon $dateTo= null): bool
    {
        $bool1 = $dateFrom ? $this->created_at >= $dateFrom : true;
        $bool2 = $dateTo ? $this->created_at <= $dateTo: true;

        return $bool1 and $bool2;
    }
}