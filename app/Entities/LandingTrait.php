<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 12.04.17
 * Time: 16:57
 */

namespace App\Entities;


trait LandingTrait
{
    protected $params;

    public function setParams(array $params = [])
    {
        $this->params = $params;
        return $this;
    }

    public function render()
    {
        return redirect()->away($this->modifiedUrl());
    }

    protected function modifiedUrl(): string
    {
        $url = $this->url;

        if ($params = $this->getParamsQuery())
            $url .= "?" . $params;

        return $url;
    }

    protected function getParamsQuery(): string
    {
        return http_build_query((array) $this->params);
    }
}