<?php

namespace App\Entities;

use App\Presenters\UserPresenter;
use App\Support\Money;
use App\Support\UserAccount;
use Exception;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laracasts\Presenter\PresentableTrait;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class User extends Authenticatable implements Transformable, ImageableInterface
{
    use Notifiable, TransformableTrait, PresentableTrait;

    protected $presenter = UserPresenter::class;

    const ADMIN = 'A';
    const CLIENT = 'C';
    public static $roles = [
        self::ADMIN  => 'Admin',
        self::CLIENT => 'Client'
    ];

    const ACTIVE = 1;
    const INACTIVE = 0;

    public static $statuses = [
        self::INACTIVE => 'Inactive',
        self::ACTIVE   => 'Active'
    ];

    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar',
        'role',
        'token',
        'active',
        'first_name',
        'last_name',
        'city',
        'state',
        'postal',
        'address',
        'phone',
        'cash_available'
    ];

    protected $hidden = [
        'password', 'remember_token', 'activation_token', 'role', 'avatar'
    ];

    protected $casts = [
        'active' => 'boolean'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function campaigns(): HasMany
    {
        return $this->hasMany(Campaign::class);
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    public function payoutRequests(): HasMany
    {
        return $this->hasMany(PayoutRequest::class);
    }

    public function socialAccounts(): HasMany
    {
        return $this->hasMany(SocialAccount::class);
    }

    public function getRoleNameAttribute(): string
    {
        return isset(self::$roles[$this->role]) ? self::$roles[$this->role] : '';
    }

    public function getActiveNameAttribute(): string
    {
        return $this->active ? 'Active' : 'Inactive';
    }

    public function setPasswordAttribute(string $value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getFullNameAttribute(): string
    {
        return $this->present()->fullName();
    }

    public function isAdmin(): bool
    {
        return $this->role == self::ADMIN;
    }

    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, 'target');
    }

    public function account()
    {
        return new UserAccount($this);
    }
}
