<?php

namespace App\Entities;

use App\Presenters\ProductPresenter;
use App\Support\Money;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Laracasts\Presenter\PresentableTrait;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Product extends Model implements Transformable, ImageableInterface
{

    use TransformableTrait, PresentableTrait;

    protected $fillable = [
        'name', 'description', 'payout'
    ];

    protected $presenter = ProductPresenter::class;

    public function campaigns(): HasMany
    {
        return $this->hasMany(Campaign::class);
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(ProductCategory::class);
    }

    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, 'target');
    }

    /**
     * @return MorphOne - this will return first pic.
     */
    public function image(): MorphOne
    {
        return $this->morphOne(Image::class, 'target');
    }
    
    public function landings(): HasMany
    {
        return $this->hasMany(Landing::class);
    }

    public function prelandings(): HasMany
    {
        return $this->hasMany(Prelanding::class);
    }

    public function payout()
    {
        return Money::fromCents($this->payout);
    }

    public function setPayoutAttribute($value)
    {
        if ($value instanceof Money)
            $value = $value->getAmount();

        $this->attributes['payout'] = (int)$value;
    }

    public function scopeWhereHasCategory(Builder $query, int $categoryId)
    {
        return $query->whereHas('categories', function (Builder $sub_query) use ($categoryId) {
            return $sub_query->where('id', $categoryId);
        });
    }

    public function scopeWhereHasCountry(Builder $query, int $countryId)
    {
        return $query->whereHas('country', function (Builder $sub_query) use ($countryId) {
            return $sub_query->where('id', $countryId);
        });
    }
}
