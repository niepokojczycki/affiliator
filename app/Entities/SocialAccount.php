<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class SocialAccount
 * @package App\Entities
 */
class SocialAccount extends Model implements Transformable {

    use TransformableTrait;

    /**
     * @var array
     * provider -> driver name, i.e. 'facebook', 'github'
     */
    protected $fillable = ['provider', 'provider_user_id'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
