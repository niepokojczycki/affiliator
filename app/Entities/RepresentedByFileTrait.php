<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 07.04.17
 * Time: 12:38
 */

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait RepresentedByFileTrait
{
    private function getDisk()
    {
        return Storage::disk('uploads');
    }

    private function getPathPrefix(): string
    {
        return 'uploads/';
    }

    protected function createNewFilename(string $original_ext): string
    {
        return implode('.', [
            Str::slug(pathinfo($this->original_name, PATHINFO_FILENAME)),
            $original_ext
        ]);
    }

    public static function createWithFile(Model $target, UploadedFile $file)
    {
        $model = (new static())->target()->associate($target);
        $model->file = $file;
        $model->save();

        return $model;
    }

    private function setFile(UploadedFile $file, $folder)
    {
        $this->original_name = $file->getClientOriginalName();
        $this->mimetype = $file->getClientMimeType();

        $original_ext = pathinfo($this->original_name, PATHINFO_EXTENSION);

        $new_filename = $this->createNewFilename($original_ext);
        $storage_path = strtolower(class_basename($this->target_type)) . "/{$this->target_id}/{$folder}/{$new_filename}";

        $disk = $this->getDisk();
        $disk->put($storage_path, file_get_contents($file->getRealPath()));

        $this->size = $disk->size($storage_path);
        $this->path = $this->getPathPrefix() . $storage_path;
    }

    public static function createManyWithFiles(Model $target, array $files)
    {
        $created = [];

        foreach ($files as $file) {
            if ($file instanceof UploadedFile and $file->isValid())
                $created[]  = self::createWithFile($target, $file);
        }

        return $created;
    }
}