<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 27.03.17
 * Time: 14:35
 */

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ProductCategory extends Model implements Transformable
{
    use TransformableTrait;

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class);
    }
}