<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 24.03.17
 * Time: 16:30
 */

namespace App\Entities;


interface ImageableInterface
{
    public function images();

}