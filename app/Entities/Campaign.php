<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 28.03.17
 * Time: 11:18
 */

namespace App\Entities;


use App\Exceptions\AlreadyCreatedException;
use App\Presenters\CampaignPresenter;
use App\Services\CampaignStatistics;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laracasts\Presenter\PresentableTrait;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Campaign extends Model implements Transformable
{

    use TransformableTrait, PresentableTrait;

    protected $presenter = CampaignPresenter::class;

    protected $fillable = [
        'postback_leads', 'postback_holds', 'postback_rejects', 'pixel_id', 'retarget_code', 'epc'
    ];

    public function awaitingLeads(): HasMany
    {
        return $this->hasMany(Lead::class)->awaiting();
    }

    public function clicks(): HasMany
    {
        return $this->hasMany(CampaignClick::class);
    }

    public function landings(): BelongsToMany
    {
        return $this->belongsToMany(Landing::class);
    }

    public function leads(): HasMany
    {
        return $this->hasMany(Lead::class);
    }

    public function prelandings(): BelongsToMany
    {
        return $this->belongsToMany(Prelanding::class);
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function rejectedLeads(): HasMany
    {
        return $this->hasMany(Lead::class)->rejected();
    }

    public function soldLeads(): HasMany
    {
        return $this->hasMany(Lead::class)->sold();
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function yearlyLeads(): HasMany
    {
        return $this->hasMany(Lead::class)->currentYear();
    }

    public static function store($input): Campaign
    {
        $campaign = new self($input);
        $user = auth()->user();
        $product_id = array_get($input, 'product_id');

        if ($user->campaigns()->where('product_id', $product_id)->count())
            throw new AlreadyCreatedException(class_basename($campaign) . 'already created');

        $campaign->product()->associate($product_id);
        $campaign->user()->associate($user);

        $campaign->save();

        $campaign->landings()->sync(array_get($input, 'landings', []));
        $campaign->prelandings()->sync(array_get($input, 'prelandings', []));

        return $campaign;
    }

    public function setRetargetCodeAttribute($value)
    {
        if (is_null($value))
            $value = '';

        $this->attributes['retarget_code'] = $value;
    }

    public function incrementClicks()
    {
        return CampaignClick::incrementOrCreate($this);
    }

    public function statistics()
    {
        return new CampaignStatistics($this);
    }

    public function updateEPC()
    {
        $clicks = $this->statistics()->clicks();

        if (!$clicks) return;

        $revenue = $this->statistics()->totalRevenue()->getInRawDollars();

        // epc by definition
        $epc = $revenue / (100 * $clicks);

        $this->update(compact('epc'));

        return $epc;
    }
}






