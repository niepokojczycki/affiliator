<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Lead extends Model implements Transformable
{

    use TransformableTrait, FilterableByDateTrait;

    const AWAITING = 0;
    const REJECTED = 1;
    const SOLD = 2;

    public static $statuses = [
        self::AWAITING => 'Awaiting',
        self::REJECTED => 'Rejected',
        self::SOLD     => 'Sold',
    ];

    protected $fillable = ['status', 'price'];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function campaign(): BelongsTo
    {
        return $this->belongsTo(Campaign::class);
    }

    public function scopeAwaiting(Builder $query): Builder
    {
        return $this->scopeStatus($query, self::AWAITING);
    }

    public function scopeRejected(Builder $query): Builder
    {
        return $this->scopeStatus($query, self::REJECTED);
    }

    public function scopeSold(Builder $query): Builder
    {
        return $this->scopeStatus($query, self::SOLD);
    }

    public function scopeStatus(Builder $query, int $status): Builder
    {
        return $query->where('status', $status);
    }

    public function scopeCurrentYear(Builder $query): Builder
    {
        $yearCarbon = Carbon::now()->startOfYear();
        return $query->where('created_at', '>=', $yearCarbon);
    }

    public function isSold(): bool
    {
        return $this->hasStatus(self::SOLD);
    }

    public function isAwaiting(): bool
    {
        return $this->hasStatus(self::AWAITING);
    }

    public function isRejected(): bool
    {
        return $this->hasStatus(self::REJECTED);
    }

    public function hasStatus(int $status): bool
    {
        return $this->status === $status;
    }
}
