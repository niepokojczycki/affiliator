<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 28.03.17
 * Time: 11:18
 */

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * This class represents an hour time frame with a number of clicks in this time frame.
 * Timestamp attribute is the beginning of that hour.
 */
class CampaignClick extends Model implements Transformable
{

    use TransformableTrait;

    protected $fillable = ['timestamp', 'clicks_count', 'campaign_id'];
    public $timestamps = false;

    public function campaign(): BelongsTo
    {
        return $this->belongsTo(Campaign::class);
    }

    public static function incrementOrCreate(Campaign $campaign)
    {
        $timestamp = Carbon::now()->minute(0)->second(0)->getTimestamp();

        $instance = self::firstOrNew([
            'timestamp'   => $timestamp,
            'campaign_id' => $campaign->id
        ]);

        $instance->increment('clicks_count');
        $instance->save();

        $campaign->updateEPC();
    }

    public function isBetweenDates(Carbon $dateFrom = null, Carbon $dateTo= null): bool
    {
        $timestampFrom = $dateFrom ? $dateFrom->timestamp : null;
        $timestampTo = $dateTo ? $dateTo->timestamp: null;

        $bool1 = $timestampFrom ? $this->timestamp >= $timestampFrom : true;
        $bool2 = $timestampTo ? $this->timestamp <= $timestampTo: true;

        return $bool1 and $bool2;
    }

}