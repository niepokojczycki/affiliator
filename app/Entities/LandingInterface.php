<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 29.03.17
 * Time: 13:39
 */

namespace App\Entities;


interface LandingInterface
{
    public function render();

    public function setParams(array $params);
}