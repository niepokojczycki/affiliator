<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 07.04.17
 * Time: 12:31
 */

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

interface RepresentedByFileInterface
{
    public function setFileAttribute(UploadedFile $file);

    public static function createWithFile(Model $target, UploadedFile $file);

    public static function createManyWithFiles(Model $target, array $files);
}