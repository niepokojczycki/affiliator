<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 24.03.17
 * Time: 15:38
 */

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Image extends Model implements Transformable, RepresentedByFileInterface
{
    use TransformableTrait, RepresentedByFileTrait;

    public function target(): MorphTo
    {
        return $this->morphTo();
    }

    public function setFileAttribute(UploadedFile $file)
    {
        $folder = "photos";

        $this->setFile($file, $folder);
    }
}