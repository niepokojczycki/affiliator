<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 28.03.17
 * Time: 11:18
 */

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Prelanding extends Model implements Transformable, LandingInterface
{
    use TransformableTrait, LandingTrait;

    protected $fillable = ['name', 'url'];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}















