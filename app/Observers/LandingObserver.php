<?php

namespace App\Observers;

use App\Entities\Landing;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 24.03.17
 * Time: 15:53
 */
class LandingObserver
{
    public function deleted(Landing $landing)
    {
        $landing->images->each->delete();
        $landing->html->delete();
        $landing->javascriptFiles->each->delete();
    }
}