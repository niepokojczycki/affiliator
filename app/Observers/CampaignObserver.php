<?php

namespace App\Observers;


use App\Entities\Campaign;
use Ramsey\Uuid\Uuid;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 28.03.17
 * Time: 15:44
 */
class CampaignObserver
{

    public function creating(Campaign $campaign)
    {
        $campaign->uuid = Uuid::uuid4()->toString();
    }
}