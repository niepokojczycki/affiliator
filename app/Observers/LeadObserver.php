<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 03.04.17
 * Time: 15:26
 */

namespace App\Observers;


use App\Entities\Lead;

class LeadObserver
{

    public function saving(Lead $lead)
    {
        if ($lead->isSold())
            $lead->campaign->updateEPC();
    }
}