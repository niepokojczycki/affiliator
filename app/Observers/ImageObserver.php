<?php

namespace App\Observers;

use App\Entities\Image;
use Croppa;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 24.03.17
 * Time: 15:53
 */
class ImageObserver
{

    public function deleted(Image $image)
    {
        Croppa::delete($image->path);
    }
}