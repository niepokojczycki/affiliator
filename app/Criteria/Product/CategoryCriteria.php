<?php

namespace App\Criteria\Product;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class CategoryCriteria implements CriteriaInterface
{
    private $categoryId;

    public function __construct(string $categoryId = null)
    {
        $this->categoryId = (int)$categoryId;
    }
    
    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->categoryId)
            return $model->whereHasCategory($this->categoryId);

        return $model;
    }
}
