<?php

namespace App\Criteria\Product;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class CountryCriteria implements CriteriaInterface
{
    private $countryId;

    public function __construct(string $countryId = null)
    {
        $this->countryId = (int)$countryId;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->countryId)
            return $model->whereHasCountry($this->countryId);

        return $model;
    }
}
