<?php

namespace App\Criteria\Product;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class NameCriteria implements CriteriaInterface
{
    private $name;

    public function __construct(string $name = null)
    {
        $this->name = $name;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->name)
            return $model->where('name', 'like', '%' . $this->name . '%');

        return $model;
    }
}
