<?php

namespace App\Criteria\Campaign;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class UuidCriteria implements CriteriaInterface
{

    private $uuid;

    public function __construct(string $uuid)
    {
        $this->uuid = $uuid;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereUuid($this->uuid);
    }
}
