<?php

namespace App\Criteria\Campaign;

use App\Entities\Product;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class ProductCriteria implements CriteriaInterface
{

    private $product;


    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->product->exists)
            return $model->whereProductId($this->product->id);

        return $model;
    }
}
