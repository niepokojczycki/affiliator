<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class SortingCriteria implements CriteriaInterface
{
    private $sortBy;
    private $order;

    public function __construct(string $sortBy, string $order)
    {
        $this->sortBy = $sortBy;
        $this->order = $order;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->orderBy($this->sortBy, $this->order);
    }
}
