<?php

namespace App\Criteria;

use App\Entities\User;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class UserCriteria implements CriteriaInterface
{

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->user->exists)
            return $model->whereUserId($this->user->id);

        return $model;
    }
}
