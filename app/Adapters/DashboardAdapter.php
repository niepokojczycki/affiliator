<?php

namespace App\Adapters;

use App\Services\Dashboard;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 31.03.17
 * Time: 15:50
 */
class DashboardAdapter
{
    private $dashboard;

    public function __construct(Dashboard $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    public function show()
    {
        $dashboard = $this->dashboard;
        $topElements = $this->dashboard->getDashboardElements();
        $conversionRatios = $this->dashboard->getConversionRatios();

        return view('dashboard.show', compact('dashboard', 'topElements', 'conversionRatios'));
    }

    public function getAjaxForMainChart()
    {
        $labels = $this->dashboard->getMainChartLabels();
        $revenues = $this->dashboard->getRevenueCountByDay();
        $clicks = $this->dashboard->getClicksCountByDay();

        return response()->json(compact('labels', 'revenues', 'clicks'));
    }
}