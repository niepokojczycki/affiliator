<?php

namespace App\Presenters;

use Croppa;
use Laracasts\Presenter\Presenter;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 27.03.17
 * Time: 17:07
 */
class ProductPresenter extends Presenter
{
    public function categoriesCount(bool $verbose = true)
    {
        $categoriesCount = $this->entity->categories->count();

        if ($categoriesCount > 1)
            return "{$categoriesCount} " . ($verbose ? "categories" : '');

        if ($categoriesCount === 1)
            return $this->entity->categories->first()->name;

        return $verbose ? "No category" : 0;
    }

    public function croppaImagePath(int $width, int $height, array $options = [])
    {
        if ($this->entity->image)
            return Croppa::url($this->entity->image->path, $width, $height, $options);

        return null;
    }

    public function imageName()
    {
        if ($this->entity->image)
            return $this->entity->image->original_name;

        return null;
    }
}