<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 27.03.17
 * Time: 17:07
 */
class CampaignPresenter extends Presenter
{
    public function trackingUrl()
    {
        if ($this->entity->exists)
            return route('campaigns.render.prelanding', ['uuid' => $this->entity->uuid]);

        return '';
    }

    public function iframeForm()
    {
        if ($this->entity->exists)
            return route('campaigns.render.prelanding', ['uuid' => $this->entity->uuid]);

        return '';
    }

    public function landingCheckbox(int $landingId)
    {
        $landingIds = $this->entity->landings->pluck('id')->all();

        if (in_array($landingId, $landingIds) or !$this->entity->exists)
            return 'checked';

        return '';
    }

    public function prelandingCheckbox(int $prelandingId)
    {
        $landingIds = $this->entity->prelandings->pluck('id')->all();

        if (in_array($prelandingId, $landingIds))
            return 'checked';

        return '';
    }
}