<?php

namespace App\Presenters;
use Laracasts\Presenter\Presenter;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 27.03.17
 * Time: 17:07
 */
class UserPresenter extends Presenter
{

    public function fullName()
    {
        return implode(' ', [$this->entity->first_name, $this->entity->last_name]);
    }
}