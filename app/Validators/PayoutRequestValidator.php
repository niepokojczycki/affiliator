<?php

namespace App\Validators;

use App\Repositories\PaymentMethodRepository;
use App\Support\Money;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 05.04.17
 * Time: 09:36
 */
class PayoutRequestValidator
{
    /**
     * @var PaymentMethodRepository
     */
    private $paymentMethodRepository;

    public function __construct(PaymentMethodRepository $paymentMethodRepository)
    {
        $this->paymentMethodRepository = $paymentMethodRepository;
    }

    public function affordableToUser($attribute, float $amount, $parameters, $validator)
    {
        $user = auth()->user();

        $withdrawAmount = Money::fromDollars($amount);
        $paymentMethod = $this->getPaymentMethod($validator);

        $totalWithdraw = Money::sum($withdrawAmount, $paymentMethod->commission());

        return $user->account()->balance()->gte($totalWithdraw);
    }

    public function satisfiesMinimum($attribute, float $amount, $parameters, $validator)
    {
        $paymentMethod = $this->getPaymentMethod($validator);

        $withdrawAmount = Money::fromDollars($amount);

        return $withdrawAmount->gte($paymentMethod->minimumAmount());
    }

    private function getPaymentMethod($validator)
    {
        $paymentMethodId = array_get($validator->getData(), 'payment_method_id');

        return $this->paymentMethodRepository->find($paymentMethodId);
    }
}













