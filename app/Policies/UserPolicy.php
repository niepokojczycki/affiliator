<?php

namespace App\Policies;


use App\Entities\User;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 06.04.17
 * Time: 10:38
 */
class UserPolicy
{

    public function admin(User $user)
    {
        return $user->isAdmin();
    }
}