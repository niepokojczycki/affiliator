<?php

namespace App\Repositories;

use Laravel\Socialite\Contracts\User as ProviderUser;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\SocialAccount;

/**
 * Class SocialAccountRepositoryEloquent
 * @package namespace App\Repositories;
 */
class SocialAccountRepositoryEloquent extends BaseRepository implements SocialAccountRepository
{
    /**
     * Specify Model class name
     */
    public function model(): string
    {
        return SocialAccount::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getAccount($providerUserId, $provider)
    {
        return $this->scopeQuery(function ($query) use ($providerUserId, $provider) {
            return $query
                ->where('provider', $provider)
                ->where('provider_user_id', $providerUserId);
        })->first();
    }

    public function createAccount($providerUserId, $provider)
    {
        return $this->create([
            'provider_user_id' => $providerUserId,
            'provider' => $provider
        ]);
    }
}
