<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductRepository
 * @package namespace App\Repositories;
 */
interface ProductRepository extends RepositoryInterface
{
    public function orderedByNameAsc();

    public function filter(array $filters);

    public function getFilteredPaginatedList($filters, $perPage = 20);

    public function getForAdminIndex($filters = [], $perPage = 20);

    public function getForSelectOptions(): Collection;
}
