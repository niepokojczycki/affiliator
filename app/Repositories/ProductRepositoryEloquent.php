<?php

namespace App\Repositories;

use App\Criteria\Product\LandingsCriteria;
use App\Criteria\SortingCriteria;
use Illuminate\Database\Eloquent\Collection;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Product;

/**
 * Class ProductRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProductRepositoryEloquent extends BaseRepository implements ProductRepository
{

    protected $criteriaNamespace = "App\\Criteria\\Product\\";
    protected $criteriaSuffix = "Criteria";

    public function model(): string
    {
        return Product::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function orderedByNameAsc()
    {
        return $this->scopeQuery(function ($query) {
            return $query->orderBy('name', 'ASC');
        })
            ->all();
    }

    public function filter(array $filters, $perPage = 10): ProductRepositoryEloquent
    {
        $filters = $this->applySorting($filters);

        foreach ($filters as $name => $value) {

            $className = $this->getClassName($name);

            if (class_exists($className))
                $this->pushCriteria(new $className($value));
        }

        return $this;
    }

    protected function getClassName(string $name): string
    {
        return $this->criteriaNamespace . ucfirst($name) . $this->criteriaSuffix;
    }


    /**
     * Because sorting needs two params, column and direction,
     * it needs to be handled outside regular flow
     * @param array $filters
     * @return array
     */
    private function applySorting(array $filters)
    {
        $direction = array_get($filters, 'order', 'asc');

        if ($sortBy = array_get($filters, 'sorting'))
            $this->pushCriteria(new SortingCriteria($sortBy, $direction));

        array_forget($filters, ['order', 'sorting']);

        return $filters;
    }

    public function getFilteredPaginatedList($filters, $perPage = 20)
    {
        $this->pushCriteria(new LandingsCriteria(1));

        return $this->filter($filters, $perPage)
            ->with('country', 'categories')
            ->paginate($perPage);
    }

    public function getForAdminIndex($filters = [], $perPage = 20)
    {
        return $this->filter($filters, $perPage)->with('country', 'categories')->paginate($perPage);
    }

    public function getForSelectOptions(): Collection
    {
        $sortBy = 'name';
        $order = 'ASC';

        $this->pushCriteria(new SortingCriteria($sortBy, $order));

        return $this->all(['id', $sortBy]);
    }
}
