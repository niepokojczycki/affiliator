<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 27.03.17
 * Time: 14:41
 */

namespace App\Repositories;


use Prettus\Repository\Contracts\RepositoryInterface;

interface ProductCategoryRepository extends RepositoryInterface
{
    public function getForSelectOptions();
}