<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 28.03.17
 * Time: 15:52
 */

namespace App\Repositories;


use App\Criteria\Campaign\ProductCriteria;
use App\Criteria\Campaign\UuidCriteria;
use App\Criteria\UserCriteria;
use App\Entities\Campaign;
use App\Entities\Product;
use App\Entities\User;
use Prettus\Repository\Eloquent\BaseRepository;

class CampaignRepositoryEloquent extends BaseRepository implements CampaignRepository
{

    public function model()
    {
        return Campaign::class;
    }

    public function getForUserAndProduct(User $user, Product $product)
    {
        $this->pushCriteria(new UserCriteria($user));
        $this->pushCriteria(new ProductCriteria($product));

        return $this->orderBy('updated_at', 'desc')->firstOrNew();
    }

    public function getForRendering($uuid)
    {
        $this->pushCriteria(new UuidCriteria($uuid));

        return $this->with(['landings', 'prelandings', 'product', 'user'])->first();
    }

    public function getForStatistics(User $user, $perPage = 10)
    {
        return $this->orderBy('created_at', 'desc')
            ->with('product', 'leads', 'clicks')
            ->paginate($perPage);
    }

    public function getForDashboard(User $user)
    {
        $this->pushCriteria(new UserCriteria($user));

        return $this->with(['product', 'yearlyLeads', 'clicks'])->all();
    }

    public function getForCSV(User $user)
    {
        $this->pushCriteria(new UserCriteria($user));

        return $this->orderBy('created_at', 'desc')
            ->with('product', 'leads', 'clicks')
            ->all();
    }
}