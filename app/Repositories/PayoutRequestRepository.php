<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 04.04.17
 * Time: 15:21
 */

namespace App\Repositories;


use App\Entities\User;
use Prettus\Repository\Contracts\RepositoryInterface;

interface PayoutRequestRepository extends RepositoryInterface
{
    public function getHistory(User $user, $perPage = 10);

    public function getActiveRequests(User $user);
}