<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 28.03.17
 * Time: 15:52
 */

namespace App\Repositories;


use App\Entities\Landing;
use Prettus\Repository\Eloquent\BaseRepository;

class LandingRepositoryEloquent extends BaseRepository implements LandingRepository
{

    public function model()
    {
        return Landing::class;
    }

    public function getForAdminIndex($perPage = 20)
    {
        return $this->with('product')->paginate($perPage);
    }
}