<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 28.03.17
 * Time: 15:52
 */

namespace App\Repositories;


use App\Entities\PaymentMethod;
use Prettus\Repository\Eloquent\BaseRepository;

class PaymentMethodRepositoryEloquent extends BaseRepository implements PaymentMethodRepository
{
    public function model()
    {
        return PaymentMethod::class;
    }

    public function getForSelect()
    {
        return $this->all();
    }
}