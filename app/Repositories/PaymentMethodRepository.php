<?php

namespace App\Repositories;

use App\Entities\Product;
use App\Entities\User;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CountryRepository
 * @package namespace App\Repositories;
 */
interface PaymentMethodRepository extends RepositoryInterface
{
    public function getForSelect();
}
