<?php

namespace App\Repositories;

use App\Criteria\SortingCriteria;
use Illuminate\Database\Eloquent\Collection;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Country;

/**
 * Class CountryRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CountryRepositoryEloquent extends BaseRepository implements CountryRepository
{

    /**
     * Specify Model class name
     */
    public function model(): string
    {
        return Country::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getForSelectOptions(): Collection
    {
        $sortBy = 'name';
        $order = 'ASC';

        $this->pushCriteria(new SortingCriteria($sortBy, $order));

        return $this->all(['id', $sortBy]);
    }
}
