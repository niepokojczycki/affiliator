<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SocialAccountRepository
 * @package namespace App\Repositories;
 */
interface SocialAccountRepository extends RepositoryInterface
{
    public function getAccount($providerUserId, $provider);

    public function createAccount($providerUserId, $provider);
}
