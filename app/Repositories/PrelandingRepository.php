<?php

namespace App\Repositories;

use App\Entities\Product;
use App\Entities\User;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CountryRepository
 * @package namespace App\Repositories;
 */
interface PrelandingRepository extends RepositoryInterface
{
    public function getForAdminIndex($perPage = 20);
}
