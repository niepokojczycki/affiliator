<?php

namespace App\Repositories;

use App\Entities\Product;
use App\Entities\User;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CountryRepository
 * @package namespace App\Repositories;
 */
interface CampaignRepository extends RepositoryInterface
{
    public function getForUserAndProduct(User $user, Product $product);

    public function getForRendering($uuid);

    public function getForStatistics(User $user);

    public function getForDashboard(User $user);

    public function getForCSV(User $user);
}
