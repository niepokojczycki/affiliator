<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 28.03.17
 * Time: 15:52
 */

namespace App\Repositories;


use App\Entities\Prelanding;
use Prettus\Repository\Eloquent\BaseRepository;

class PrelandingRepositoryEloquent extends BaseRepository implements PrelandingRepository
{

    public function model()
    {
        return Prelanding::class;
    }

    public function getForAdminIndex($perPage = 20)
    {
        return $this->with('product')->paginate($perPage);
    }
}