<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 27.03.17
 * Time: 14:41
 */

namespace App\Repositories;


use App\Criteria\SortingCriteria;
use App\Entities\ProductCategory;
use Illuminate\Database\Eloquent\Collection;
use Prettus\Repository\Eloquent\BaseRepository;

class ProductCategoryRepositoryEloquent extends BaseRepository implements ProductCategoryRepository
{
    public function model()
    {
        return ProductCategory::class;
    }

    public function getForSelectOptions(): Collection
    {
        $sortBy = 'name';
        $order = 'ASC';

        $this->pushCriteria(new SortingCriteria($sortBy, $order));

        return $this->all(['id', $sortBy]);
    }

}