<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 04.04.17
 * Time: 15:21
 */

namespace App\Repositories;


use App\Criteria\UserCriteria;
use App\Entities\PayoutRequest;
use App\Entities\User;
use Prettus\Repository\Eloquent\BaseRepository;

class PayoutRequestRepositoryEloquent extends BaseRepository implements PayoutRequestRepository
{
    public function model()
    {
        return PayoutRequest::class;
    }

    public function getActiveRequests(User $user, $perPage = 10)
    {
        $this->pushCriteria(new UserCriteria($user));

        return $this->orderBy('created_at', 'DESC')
            ->scopeQuery(function ($query) {
                return $query->where('status', PayoutRequest::AWAITING);
            })
            ->with('paymentMethod')
            ->all();
    }

    public function getHistory(User $user, $perPage = 10)
    {
        $this->pushCriteria(new UserCriteria($user));

        return $this->with('paymentMethod')
            ->orderBy('updated_at', 'DESC')
            ->scopeQuery(function ($query) {
                return $query->where('status', PayoutRequest::PAID);
            })
            ->paginate($perPage);
    }
}