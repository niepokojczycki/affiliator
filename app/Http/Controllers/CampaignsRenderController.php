<?php

namespace App\Http\Controllers;


use App\Entities\Landing;
use App\Services\CampaignRenderer;
use Illuminate\Http\Request;

class CampaignsRenderController extends Controller
{

    /**
     * @var CampaignRenderer
     */
    private $campaignRenderer;

    public function __construct(CampaignRenderer $campaignRenderer)
    {
        $this->campaignRenderer = $campaignRenderer;
    }

    public function renderPrelanding($uuid)
    {
        return $this->campaignRenderer->render($uuid);
    }

    public function renderLanding($uuid, Landing $landing)
    {
        return $this->campaignRenderer->render($uuid, $landing);
    }

    public function getDetails(Request $request)
    {
        // TODO: change this!

        $landing = Landing::find(array_get($request, 'landing_id'));
        $uuid = array_get($request, 'uuid');

        $landing_url = route('campaigns.render.landing', compact('uuid', 'landing'));

        return response()->json(compact('landing_url'));
    }
}
