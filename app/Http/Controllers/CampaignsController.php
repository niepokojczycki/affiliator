<?php

namespace App\Http\Controllers;


use App\Entities\Campaign;
use App\Http\Requests\CampaignStoreRequest;
use App\Http\Requests\CampaignUpdateRequest;
use Exception;
use Illuminate\Http\Request;

class CampaignsController extends Controller
{

    public function store(CampaignStoreRequest $request)
    {
        $input = $this->getSanitizedInput($request);

        try {
            $campaign = Campaign::store(array_filter($input));
            $trackingUrl = $campaign->present()->trackingUrl();

            $updateUrl = route('campaigns.update', compact('campaign'));

            return response()->json(compact('campaign', 'trackingUrl', 'updateUrl'));
        } catch (Exception $e) {
            $message = $e->getMessage();
            return response()->json(compact('message'), 400);
        }
    }

    public function update(CampaignUpdateRequest $request, Campaign $campaign)
    {
        $input = $this->getSanitizedInput($request);

        try {
            $campaign->update($input);

            $campaign->landings()->sync(array_get($input, 'landings', []));
            $campaign->prelandings()->sync(array_get($input, 'prelandings', []));
            $trackingUrl = $campaign->present()->trackingUrl();

            return response()->json(compact('campaign', 'trackingUrl'));
        } catch (Exception $e) {
            $message = $e->getMessage();
            return response()->json(compact('message'), 422);
        }

    }

    protected function getSanitizedInput(Request $request)
    {
        return array_filter($request->only(
            'product_id',
            'landings',
            'prelandings',
            'postback_leads',
            'postback_holds',
            'postback_rejects',
            'pixel_id',
            'retarget_code'));
    }
}
