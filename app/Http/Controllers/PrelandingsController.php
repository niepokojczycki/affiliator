<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 28.03.17
 * Time: 13:39
 */

namespace App\Http\Controllers;


use App\Entities\Prelanding;

class PrelandingsController extends Controller
{
    public function preview(Prelanding $prelanding)
    {
        return $prelanding->render();
    }
}