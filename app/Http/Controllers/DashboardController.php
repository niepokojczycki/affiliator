<?php

namespace App\Http\Controllers;


use App\Adapters\DashboardAdapter;

class DashboardController extends Controller
{
    public function show(DashboardAdapter $adapter)
    {
        return $adapter->show();
    }

    public function getAjaxForMainChart(DashboardAdapter $adapter)
    {
        return $adapter->getAjaxForMainChart();
    }
}