<?php

namespace App\Http\Controllers;


use App\Repositories\CampaignRepository;
use App\Services\CSVDownloader;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Response;

class StatisticsController extends Controller
{
    private $campaignRepository;

    public function __construct(CampaignRepository $campaignRepository)
    {
        $this->campaignRepository = $campaignRepository;
    }

    public function show()
    {
        $campaigns = $this->campaignRepository->getForStatistics(auth()->user());

        return view('statistics.show', compact('campaigns'));
    }

    public function download(CSVDownloader $downloader)
    {
        $campaigns = $this->campaignRepository->getForCSV(auth()->user());

        $filename = 'campaigns'.Carbon::now()->toDateTimeString().'.csv';

        $data = $this->getCampaignsData($campaigns);

        return $downloader->download($filename, $data);
    }

    private function getCampaignsData(Collection $campaigns): array
    {
        $data = [];
        $i = 0;
        foreach ($campaigns as $campaign) {
            $data[] = [
                ""                     => ++$i,
                "Product name"         => $campaign->product->name,
                "Clicks count"         => $campaign->statistics()->clicks(),
                "Leads count"          => $campaign->statistics()->leads(),
                "Sales count"          => $campaign->statistics()->sales(),
                "Awaiting leads count" => $campaign->statistics()->awaiting(),
                "Rejected leads count" => $campaign->statistics()->rejects(),
                "Camapaign EPC"        => $campaign->statistics()->epc(),
                "Revenue from leads"   => $campaign->statistics()->leadsRevenue()->inDollars(),
                "Revenue from sales"   => $campaign->statistics()->salesRevenue()->inDollars(),
                "Revenue from holds"   => $campaign->statistics()->holdsRevenue()->inDollars(),
                "Total revenue"        => $campaign->statistics()->totalRevenue()->inDollars(),
            ];
        }

        # add headers for each column in the CSV download
        if(sizeof($data))
            array_unshift($data, array_keys($data[0]));

        return $data;
    }
}
