<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 04.04.17
 * Time: 15:21
 */

namespace App\Http\Controllers;


use App\Entities\PayoutRequest;
use App\Http\Requests\PayoutRequestStoreRequest;
use App\Repositories\PaymentMethodRepository;
use App\Repositories\PayoutRequestRepository;
use App\Repositories\UserRepository;
use Exception;

class PayoutRequestsController
{
    const PER_PAGE = 10;

    protected $userRepository;
    protected $paymentMethodRepository;
    protected $payoutRequestsRepository;

    public function __construct(
        PayoutRequestRepository $payoutRequestsRepository,
        UserRepository $userRepository,
        PaymentMethodRepository $paymentMethodRepository)
    {
        $this->payoutRequestsRepository = $payoutRequestsRepository;
        $this->userRepository = $userRepository;
        $this->paymentMethodRepository = $paymentMethodRepository;
    }


    public function index()
    {
        $user = $this->userRepository->getAuthorized();
        $payoutRequests = $this->payoutRequestsRepository->getActiveRequests($user);
        $paymentMethods = $this->paymentMethodRepository->getForSelect();
        $payoutsHistory = $this->payoutRequestsRepository->getHistory($user, self::PER_PAGE);

        return view('payouts.index', compact('payoutRequests', 'user', 'paymentMethods', 'payoutsHistory'));
    }

    public function store(PayoutRequestStoreRequest $request)
    {
        return $request->handle($this->paymentMethodRepository);
    }

    /**
     * @param PayoutRequest $payoutRequest
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cancel(PayoutRequest $payoutRequest)
    {
        try {
            $payoutRequest->cancel();

            return $this->successResponse('Payout request canceled');

        } catch (Exception $exception) {
            return $this->failedResponse($exception);
        }
    }

    public function failedResponse(Exception $exception)
    {
        return redirect()
            ->back()
            ->withInput()
            ->with('message', [
                'message' => $exception->getMessage(),
                'type'    => 'error'
            ]);
    }

    public function successResponse($message)
    {
        return redirect()->back()->with('message', [
            'message' => $message,
            'type'    => 'success'
        ]);
    }
}