<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 28.03.17
 * Time: 13:39
 */

namespace App\Http\Controllers;


use App\Entities\Landing;

class LandingsController extends Controller
{
    public function preview(Landing $landing)
    {
        return $landing->render();
    }
}