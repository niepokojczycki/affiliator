<?php

namespace App\Http\Controllers;

use App\Entities\Product;
use App\Repositories\CampaignRepository;
use App\Repositories\CountryRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    const PER_PAGE = 20;

    protected $productRepository;
    protected $countryRepository;
    protected $categoryRepository;

    public function __construct(
        ProductRepository $productRepository,
        CountryRepository $countryRepository,
        ProductCategoryRepository $categoryRepository)
    {
        $this->productRepository = $productRepository;
        $this->countryRepository = $countryRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function index(Request $request)
    {
        $products = $this->productRepository->getFilteredPaginatedList($request->all(), self::PER_PAGE);
        $countries = $this->countryRepository->getForSelectOptions();
        $categories = $this->categoryRepository->getForSelectOptions();

        return view('products.index', compact('products', 'countries', 'categories'));
    }

    public function show(Product $product, CampaignRepository $campaignRepository)
    {
        $product->load('country', 'categories', 'images');

        $campaign = $campaignRepository->getForUserAndProduct(auth()->user(), $product);

        $formAction = $campaign->exists
            ? route('campaigns.update', compact('campaign'))
            : route('campaigns.store');

        return view('products.show', compact('product', 'campaign', 'formAction'));
    }

}
