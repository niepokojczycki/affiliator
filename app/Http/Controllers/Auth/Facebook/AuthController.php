<?php

namespace App\Http\Controllers\Auth\Facebook;

use App\Services\SocialAccountService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

/**
 * Class AuthController
 * @package App\Http\Controllers\Auth\Facebook
 */
class AuthController extends Controller {

    protected $provider = 'facebook';
    /**
     * @return mixed
     */
    public function redirectToProvider()
    {
        return Socialite::driver($this->provider)->redirect();
    }


    /**
     * @param SocialAccountService $socialAccountService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleProviderCallback(SocialAccountService $socialAccountService)
    {
        $user = $socialAccountService->createOrGetUser(Socialite::driver($this->provider)->user(), $this->provider);
        if ($user) {
            Auth::login($user);
        }

        return redirect()->route('dashboard.show');
    }
}