<?php

namespace App\Http\Controllers\Auth;

use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Repositories\CountryRepository;
use App\Services\RegularUserService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo;
    /**
     * @var CountryRepository
     */
    private $countryRepository;

    /**
     * Create a new controller instance.
     * @param CountryRepository $countryRepository
     */
    public function __construct(CountryRepository $countryRepository)
    {
        $this->redirectTo = route('dashboard.show');
        $this->middleware('guest');

        $this->countryRepository = $countryRepository;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $countries = $this->countryRepository->all()->pluck('name', 'id')->toArray();

        return view('auth.register', compact('countries'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, $this->rules());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        $regularUserService = app(RegularUserService::class);

        /** @var User $user */
        $user = $regularUserService->register($data);
        $user->fill(collect($data)->except(['name', 'email', 'password', 'id', 'activation_token', 'active', 'token', 'role'])->toArray());

        $country = $this->countryRepository->find(array_get($data, 'country_id'));

        $user->country()->associate($country);
        $user->save();

        return $user;
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return [
            'first_name'            => 'required|min:3|max:255',
            'last_name'             => 'required|min:3|max:255',
            'email'                 => 'required|email|unique:users',
            'password'              => 'required|min:5|confirmed',
            'password_confirmation' => 'required|min:5',
            'country_id'            => 'required',
            'city'                  => 'required|min:3',
            'state'                 => 'required|min:3',
            'postal'                => 'required|min:3',
            'address'               => 'required|min:3',
            'phone'                 => 'required|min:3',
        ];
    }
}
