<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 06.04.17
 * Time: 11:05
 */

namespace App\Http\Controllers\Admin;


use App\Entities\Product;
use App\Http\Requests\Admin\Product\ProductStoreRequest;
use App\Http\Requests\Admin\Product\ProductUpdateRequest;
use App\Repositories\CountryRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductRepository;

class ProductsController extends AdminController
{

    private $productRepository;
    private $productCategoryRepository;
    private $countryRepository;

    public function __construct(
        ProductRepository $productRepository,
        ProductCategoryRepository $productCategoryRepository,
        CountryRepository $countryRepository)
    {
        parent::__construct();

        $this->productRepository = $productRepository;
        $this->productCategoryRepository = $productCategoryRepository;
        $this->countryRepository = $countryRepository;
    }

    public function index()
    {
        $products = $this->productRepository->getForAdminIndex();

        return view('admin.products.index', compact('products'));
    }

    public function show(Product $product)
    {
        $product->load('landings', 'prelandings');
        return view('admin.products.show', compact('product'));
    }

    public function create()
    {
        $countries = $this->countryRepository->getForSelectOptions();
        $categories = $this->productCategoryRepository->getForSelectOptions();

        $product = new Product();

        return view('admin.products.create', compact('countries', 'categories', 'product'));
    }

    public function store(ProductStoreRequest $request)
    {
        $product = $request->storeProduct();

        return redirect()->route('admin.products.show', compact('product'));
    }

    public function edit(Product $product)
    {
        $countries = $this->countryRepository->getForSelectOptions();
        $categories = $this->productCategoryRepository->getForSelectOptions();

        return view('admin.products.edit', compact('countries', 'categories', 'product'));
    }

    public function update(Product $product, ProductUpdateRequest $request)
    {
        $request->updateProduct($product);

        return redirect()->route('admin.products.show', compact('product'));
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('admin.products.index');
    }
}