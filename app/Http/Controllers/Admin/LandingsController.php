<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 06.04.17
 * Time: 11:05
 */

namespace App\Http\Controllers\Admin;


use App\Entities\Landing;
use App\Http\Requests\Admin\LandingInterfaceUpdateRequest;
use App\Http\Requests\Admin\LandingStoreRequest;
use App\Repositories\LandingRepository;
use App\Repositories\ProductRepository;

class LandingsController extends AdminController
{

    const PER_PAGE = 30;

    private $landingRepository;
    private $productRepository;

    public function __construct(LandingRepository $landingRepository, ProductRepository $productRepository)
    {
        parent::__construct();

        $this->landingRepository = $landingRepository;
        $this->productRepository = $productRepository;
    }

    public function index()
    {
        $landings = $this->landingRepository->getForAdminIndex(self::PER_PAGE);

        return view('admin.landings.index', compact('landings'));
    }

    public function show(Landing $landing)
    {
        return view('admin.landings.show', compact('landing'));
    }

    public function create()
    {
        $landingable = new Landing();
        $formAction = route('admin.landings.store');
        $products = $this->productRepository->getForSelectOptions();

        return view('admin.landings.create', compact('landingable', 'formAction', 'products'));
    }

    public function store(LandingStoreRequest $request)
    {
        $landing = $request->storeLanding();

        return redirect()->route('admin.landings.show', compact('landing'));
    }

    public function edit(Landing $landing)
    {
        $landingable = $landing;
        $formAction = route('admin.landings.update', compact('landing'));
        $products = $this->productRepository->getForSelectOptions();

        return view('admin.landings.edit', compact('landingable', 'formAction', 'products'));
    }

    public function update(Landing $landing, LandingInterfaceUpdateRequest $request)
    {
        $request->updateLanding($landing);

        return redirect()->route('admin.landings.show', compact('landing'));
    }

    public function destroy(Landing $landing)
    {
        $landing->delete();

        return redirect()->back();
    }
}