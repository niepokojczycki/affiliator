<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 06.04.17
 * Time: 11:05
 */

namespace App\Http\Controllers\Admin;


use App\Entities\Campaign;

class CampaignsController extends AdminController
{

    public function index()
    {
        //
    }

    public function show(Campaign $campaign)
    {
        //
    }

    public function create()
    {
        //
    }

    public function store()
    {
        //
    }

    public function edit(Campaign $campaign)
    {
        //
    }

    public function update(Campaign $campaign)
    {
        //
    }

    public function destroy(Campaign $campaign)
    {
        //
    }
}