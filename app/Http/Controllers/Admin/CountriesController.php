<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 06.04.17
 * Time: 11:05
 */

namespace App\Http\Controllers\Admin;


use App\Entities\Campaign;
use App\Entities\Country;

class CountriesController extends AdminController
{

    public function index()
    {
        //
    }

    public function show(Country $country)
    {
        //
    }

    public function create()
    {
        //
    }

    public function store()
    {
        //
    }

    public function edit(Country $country)
    {
        //
    }

    public function update(Country $country)
    {
        //
    }

    public function destroy(Country $country)
    {
        //
    }
}