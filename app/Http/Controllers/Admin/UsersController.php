<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 06.04.17
 * Time: 11:05
 */

namespace App\Http\Controllers\Admin;


use App\Entities\User;

class UsersController extends AdminController
{

    public function index()
    {
        //
    }

    public function show(User $user)
    {
        //
    }

    public function create()
    {
        //
    }

    public function store()
    {
        //
    }

    public function edit(User $user)
    {
        //
    }

    public function update(User $user)
    {
        //
    }

    public function destroy(User $user)
    {
        //
    }
}