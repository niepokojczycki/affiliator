<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 05.04.17
 * Time: 16:57
 */
class AdminController extends Controller
{

    public function __construct()
    {
        $adminPaths = [
            "Campaigns"   => route('admin.campaigns.index'),
            "Countries"   => route('admin.countries.index'),
            "Landings"    => route('admin.landings.index'),
            "Prelandings" => route('admin.prelandings.index'),
            "Products"    => route('admin.products.index'),
            "Users"       => route('admin.users.index'),
        ];

        $adminPanel = true;

        view()->share('adminPaths', $adminPaths);
        view()->share('adminPanel', $adminPanel);
    }

    public function index()
    {
        return view('admin.index');
    }
}