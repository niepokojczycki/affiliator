<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 06.04.17
 * Time: 11:05
 */

namespace App\Http\Controllers\Admin;


use App\Entities\Prelanding;
use App\Http\Requests\Admin\LandingInterfaceUpdateRequest;
use App\Http\Requests\Admin\PrelandingStoreRequest;
use App\Repositories\PrelandingRepository;
use App\Repositories\ProductRepository;

class PrelandingsController extends AdminController
{
    const PER_PAGE = 30;

    private $prelandingRepository;
    private $productRepository;

    public function __construct(PrelandingRepository $prelandingRepository, ProductRepository $productRepository)
    {
        parent::__construct();

        $this->prelandingRepository = $prelandingRepository;
        $this->productRepository = $productRepository;
    }

    public function index()
    {
        $prelandings = $this->prelandingRepository->getForAdminIndex(self::PER_PAGE);

        return view('admin.prelandings.index', compact('prelandings'));
    }

    public function show(Prelanding $prelanding)
    {
        return view('admin.prelandings.show', compact('prelanding'));
    }

    public function create()
    {
        $landingable = new Prelanding();
        $formAction = route('admin.prelandings.store');
        $products = $this->productRepository->getForSelectOptions();

        return view('admin.prelandings.create', compact('landingable', 'formAction', 'products'));
    }

    public function store(PrelandingStoreRequest $request)
    {
        $prelanding = $request->storeLanding();

        return redirect()->route('admin.prelandings.show', compact('prelanding'));
    }

    public function edit(Prelanding $prelanding)
    {
        $landingable = $prelanding;
        $formAction = route('admin.prelandings.update', compact('prelanding'));
        $products = $this->productRepository->getForSelectOptions();

        return view('admin.prelandings.edit', compact('landingable', 'formAction', 'products'));
    }

    public function update(Prelanding $prelanding, LandingInterfaceUpdateRequest $request)
    {
        $request->updateLanding($prelanding);

        return redirect()->route('admin.prelandings.show', compact('prelanding'));
    }

    public function destroy(Prelanding $prelanding)
    {
        $prelanding->delete();

        return redirect()->back();
    }
}