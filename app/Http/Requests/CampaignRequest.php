<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 29.03.17
 * Time: 16:04
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class CampaignRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        // TODO: add validation rules for fields
        return [
            'product_id' => 'required',
            'landings' => 'required',
//            'postback_leads' => ''
//            'postback_holds' => ''
//            'postback_rejects' => ''
//            'pixed_id' => ''
//            'retarget_code' => ''
        ];
    }

    public function messages()
    {
        return [
            'landings.required' => 'Choose at least one landing',
        ];
    }
}