<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 10.04.17
 * Time: 11:11
 */

namespace App\Http\Requests\Admin\Product;


use App\Support\Money;
use Illuminate\Foundation\Http\FormRequest;

abstract class ProductRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'name'    => 'required',
            'payout'  => 'required|numeric',
            'country' => 'required|numeric',
            'image'   => 'image'
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    protected function getData(): array
    {
        $attributes = $this->only('name', 'description');
        $countryId = $this->get('country');
        $categoryIds = $this->get('categories');
        $payout = Money::fromDollars($this->get('payout'))->getAmount();
        return array($attributes, $countryId, $categoryIds, $payout);
    }
}