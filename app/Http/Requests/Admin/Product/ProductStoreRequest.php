<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 29.03.17
 * Time: 16:04
 */

namespace App\Http\Requests\Admin\Product;


use App\Entities\Image;
use App\Entities\Product;

class ProductStoreRequest extends ProductRequest
{
    public function storeProduct()
    {
        list($attributes, $countryId, $categoryIds, $payout) = $this->getData();

        $product = new Product($attributes);
        $product->payout = $payout;

        $product->country()->associate($countryId);
        $product->save();

        $product->categories()->sync($categoryIds);
        $product->save();

        if ($image = $this->file('image'))
            Image::createWithFile($product, $image);

        return $product;
    }

}