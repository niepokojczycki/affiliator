<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 29.03.17
 * Time: 16:04
 */

namespace App\Http\Requests\Admin\Product;


use App\Entities\Image;
use App\Entities\Product;

class ProductUpdateRequest extends ProductRequest
{
    public function updateProduct(Product $product)
    {
        list($attributes, $countryId, $categoryIds, $payout) = $this->getData();
        $product->fill($attributes);

        $product->payout = $payout;

        $product->country()->associate($countryId);

        $product->categories()->sync($categoryIds);
        $product->save();

        if ($image = $this->file('image')) {
            $product->images()->delete();
            Image::createWithFile($product, $image);
        }

        return $product;
    }
}