<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 07.04.17
 * Time: 15:47
 */

namespace App\Http\Requests\Admin;


use App\Entities\LandingInterface;

class LandingInterfaceUpdateRequest extends LandingInterfaceRequest
{
    public function updateLanding(LandingInterface $landing)
    {
        $attributes = $this->only('name', 'url');

        $landing->fill($attributes);

        $landing->product()->associate($this->get('product'));
        $landing->save();

        return $landing;
    }
}