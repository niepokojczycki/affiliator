<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 07.04.17
 * Time: 15:47
 */

namespace App\Http\Requests\Admin;


use App\Entities\LandingInterface;

abstract class LandingInterfaceStoreRequest extends LandingInterfaceRequest
{
    public function storeLanding()
    {
        $landing = $this->newLandingInterface($this->only('name', 'url'));
        $landing->product()->associate($this->get('product'));
        $landing->save();

        return $landing;
    }

    protected abstract function newLandingInterface(array $attributes): LandingInterface;
}