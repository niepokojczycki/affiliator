<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 29.03.17
 * Time: 16:04
 */

namespace App\Http\Requests\Admin;


use App\Entities\Landing;
use App\Entities\LandingInterface;

class LandingStoreRequest extends LandingInterfaceStoreRequest
{
    protected function newLandingInterface(array $attributes): LandingInterface
    {
        return new Landing($attributes);
    }
}