<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 07.04.17
 * Time: 15:47
 */

namespace App\Http\Requests\Admin;


use Illuminate\Foundation\Http\FormRequest;

abstract class LandingInterfaceRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'name'    => 'required',
            'url'     => 'required|url',
            'product' => 'required|numeric',
        ];
    }
}