<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 29.03.17
 * Time: 16:04
 */

namespace App\Http\Requests\Admin;

use App\Entities\LandingInterface;
use App\Entities\Prelanding;

class PrelandingStoreRequest extends LandingInterfaceStoreRequest
{

    protected function newLandingInterface(array $attributes): LandingInterface
    {
        return new Prelanding($attributes);
    }
}