<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 05.04.17
 * Time: 09:19
 */

namespace App\Http\Requests;


use App\Entities\PayoutRequest;
use App\Repositories\PaymentMethodRepository;
use App\Support\Money;
use Illuminate\Foundation\Http\FormRequest;

class PayoutRequestStoreRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'amount' => 'user_can_afford|minimum_amount'
        ];
    }

    public function messages()
    {
        return [
            'amount.user_can_afford' => "You don't have enough funds",
            'amount.minimum_amount'  => "You can't withdraw less than minimum"
        ];
    }

    public function handle(PaymentMethodRepository $paymentMethodRepository)
    {
        $user = $this->user();
        $amount = Money::fromDollars($this->input('amount'));

        $paymentMethod = $paymentMethodRepository->find($this->input('payment_method_id'));

        PayoutRequest::store($user, $amount, $paymentMethod);

        return redirect()->back()->with('message', [
            'message' => "Payout request created",
            'type'    => 'success'
        ]);
    }
}