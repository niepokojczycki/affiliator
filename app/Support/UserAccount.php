<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 05.04.17
 * Time: 11:12
 *
 * Class representing financial account of a user.
 */

namespace App\Support;


use App\Entities\User;

class UserAccount
{
    /** @var Money */
    protected $balance;
    /** @var User */
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->balance = Money::fromCents($user->cash_available);
    }

    public function balance(): Money
    {
        return $this->balance;
    }

    public function withdraw(Money $money): UserAccount
    {
        $this->balance->subtract($money);
        $this->save();

        return $this;
    }

    public function add(Money $money): UserAccount
    {
        $this->balance->add($money);
        $this->save();

        return $this;
    }

    private function save()
    {
        $this->user->update(['cash_available' => $this->balance->getAmount()]);
    }
}