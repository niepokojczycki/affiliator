<?php
/**
 * User: Grzegorz
 * Date: 2016-12-13
 * Time: 17:09
 */

namespace App\Support;

use App\Entities\Product;
use App\Entities\User;

/**
 * Class Support
 * @package App\Support
 */
class Support {

    /**
     * @return string
     */
    public static function generateToken()
    {
        return strtoupper(str_random(8));
    }

    /**
     * @param User $user
     * @param Product $product
     * @return string
     */
    public static function createRef(User $user, Product $product)
    {
        return implode('-', [$user->token, $product->token, str_random(8)]);
    }


    /**
     * @param $number
     * @param string $currency
     * @return string
     */
    public static function formatMoney($number, $currency = 'dollar')
    {
        switch ($currency)
        {
            case 'dollar':
                return money_format('%.2n', $number);
                break;
            default:
                return number_format($number, 2);
        }
    }

}