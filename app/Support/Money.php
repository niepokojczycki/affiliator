<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 21.03.17
 * Time: 14:47
 */

namespace App\Support;

use Exception;

class Money
{
    /** @var  int */
    private $amount;

    /**
     * Money constructor.
     * @param int $amount Value in cents
     */
    public function __construct(int $amount)
    {
        $this->amount = $amount;
    }

    public static function sum(Money... $money)
    {
        $value = 0;
        foreach ($money as $singleMoney)
            $value += $singleMoney->getAmount();

        return new self($value);
    }

    public static function fromCents(int $cents)
    {
        return new self($cents);
    }

    public static function fromDollars(float $dollars)
    {
        $cents = $dollars * 100;

        return new self((int)$cents);
    }

    public function inDollars()
    {
        $dollars = $this->amount / 100;

        return '$' . number_format($dollars, 2, '.', ',');
    }

    public function getInRawDollars()
    {
        return $this->amount / 100;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function gt(Money $money)
    {
        return $this->amount > $money->getAmount();
    }

    public function equals(Money $money)
    {
        return $this->amount === $money->getAmount();
    }

    public function gte(Money $money)
    {
        return $this->gt($money) or $this->equals($money);
    }

    public function lte(Money $money)
    {
        return $this->lt($money) or $this->equals($money);
    }

    public function lt(Money $money)
    {
        return !$this->gt($money);
    }

    public function add(Money $money): Money
    {
        $this->amount += $money->getAmount();
        return $this;
    }

    public function subtract(Money $money): Money
    {
        $newAmount = $this->amount - $money->getAmount();

        if ($newAmount < 0)
            throw new Exception("Money can't have negative value");

        $this->amount = $newAmount;
        return $this;
    }

    public function copy(): Money
    {
        return clone $this;
    }
}