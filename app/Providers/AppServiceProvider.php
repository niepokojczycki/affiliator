<?php

namespace App\Providers;

use App\Entities\Campaign;
use App\Entities\HtmlFile;
use App\Entities\Image;
use App\Entities\JavascriptFile;
use App\Entities\Landing;
use App\Entities\Lead;
use App\Observers\CampaignObserver;
use App\Observers\HtmlFileObserver;
use App\Observers\ImageObserver;
use App\Observers\JavascriptFileObserver;
use App\Observers\LandingObserver;
use App\Observers\LeadObserver;
use App\Services\CampaignGroupStatistics;
use App\Services\Dashboard;
use App\Validators\LandingValidator;
use App\Validators\PayoutRequestValidator;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Lead::observe(LeadObserver::class);
        Image::observe(ImageObserver::class);
        Landing::observe(LandingObserver::class);
        Campaign::observe(CampaignObserver::class);

        Validator::extend('user_can_afford', PayoutRequestValidator::class . "@affordableToUser");
        Validator::extend('minimum_amount', PayoutRequestValidator::class . "@satisfiesMinimum");
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
        }

        $this->app->bind('dashboard', Dashboard::class);
        $this->app->bind('campaignGroupStatistics', CampaignGroupStatistics::class);
    }
}
