<?php

namespace App\Providers;

use App\Repositories\CampaignRepository;
use App\Repositories\CampaignRepositoryEloquent;
use App\Repositories\CountryRepository;
use App\Repositories\CountryRepositoryEloquent;
use App\Repositories\LandingRepository;
use App\Repositories\LandingRepositoryEloquent;
use App\Repositories\PaymentMethodRepository;
use App\Repositories\PaymentMethodRepositoryEloquent;
use App\Repositories\PayoutRequestRepository;
use App\Repositories\PayoutRequestRepositoryEloquent;
use App\Repositories\PrelandingRepository;
use App\Repositories\PrelandingRepositoryEloquent;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductCategoryRepositoryEloquent;
use App\Repositories\ProductRepository;
use App\Repositories\ProductRepositoryEloquent;
use App\Repositories\SocialAccountRepository;
use App\Repositories\SocialAccountRepositoryEloquent;
use App\Repositories\UserRepository;
use App\Repositories\UserRepositoryEloquent;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $repositories = [
            UserRepository::class            => UserRepositoryEloquent::class,
            SocialAccountRepository::class   => SocialAccountRepositoryEloquent::class,
            CountryRepository::class         => CountryRepositoryEloquent::class,
            ProductRepository::class         => ProductRepositoryEloquent::class,
            ProductCategoryRepository::class => ProductCategoryRepositoryEloquent::class,
            CampaignRepository::class        => CampaignRepositoryEloquent::class,
            PayoutRequestRepository::class   => PayoutRequestRepositoryEloquent::class,
            PaymentMethodRepository::class   => PaymentMethodRepositoryEloquent::class,
            LandingRepository::class         => LandingRepositoryEloquent::class,
            PrelandingRepository::class      => PrelandingRepositoryEloquent::class,
        ];

        foreach ($repositories as $abstract => $implementation)
            $this->app->bind($abstract, $implementation);
    }
}
