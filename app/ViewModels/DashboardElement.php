<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 30.03.17
 * Time: 10:33
 */


namespace App\ViewModels;


use App\Support\Money;

class DashboardElement
{
    private $title;
    private $count;
    private $weekly;
    private $monetary;

    public function __construct(string $title, int $count, bool $weekly = true, $monetary = false)
    {
        $this->title = $title;
        $this->count = $count;
        $this->weekly = $weekly;
        $this->monetary = $monetary;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function presentCount(): string
    {
        $number = $this->monetary ? (new Money($this->count))->inDollars() : $this->count;
        $suffix = $this->weekly ? " / week" : "";

        return $number . $suffix;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}