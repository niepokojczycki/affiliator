<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 03.04.17
 * Time: 10:12
 */

namespace App\ViewModels;


class ConversionRatio
{

    private $partialCount;
    private $totalCount;
    private $title;

    public function __construct(string $title, int $partialCount = 0, int $totalCount = 0)
    {
        $this->partialCount = $partialCount;
        $this->totalCount = $totalCount;
        $this->title = $title;
    }

    public function getPartialCount(): int
    {
        return $this->partialCount;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

}