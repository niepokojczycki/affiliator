<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 03.04.17
 * Time: 16:20
 */

namespace App\Services;


use App\Support\Money;
use Carbon\Carbon;

class CampaignGroupStatistics implements CampaignStatisticsInterface
{
    protected $dateFrom;
    protected $dateTo;
    protected $campaigns;

    public function __construct($campaigns = null, Carbon $dateFrom = null, Carbon $dateTo = null)
    {
        $this->campaigns = $campaigns ?? collect();
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
    }

    public function setDateFrom(Carbon $dateFrom = null): CampaignGroupStatistics
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    public function setDateTo(Carbon $dateTo = null): CampaignGroupStatistics
    {
        $this->dateTo = $dateTo;
        return $this;
    }

    public function getLeads()
    {
        return $this->campaigns
            ->map->leads->flatten()
            ->filter->isBetweenDates($this->dateFrom, $this->dateTo);
    }

    public function getClicks()
    {
        return $this->campaigns
            ->map->clicks->flatten()
            ->filter->isBetweenDates($this->dateFrom, $this->dateTo);
    }

    public function clicks(): int
    {
        return $this->getClicks()->sum('clicks_count');
    }

    public function leads(): int
    {
        return $this->getLeads()->count();
    }

    public function sales(): int
    {
        return $this->getLeads()->filter->isSold()->count();
    }

    public function awaiting(): int
    {
        return $this->getLeads()->filter->isAwaiting()->count();
    }

    public function rejects(): int
    {
        return $this->getLeads()->filter->isRejected()->count();
    }


    public function salesRevenue(): Money
    {
        $sales_revenues = $this->getLeads()
            ->filter->isSold()
            ->map->price
            ->sum();

        return new Money($sales_revenues);
    }

    public function leadsRevenue(): Money
    {
        //todo: change this when implementing different kind of payouts available
        return new Money(0);
    }

    public function holdsRevenue(): Money
    {
        //todo: change this when implementing different kind of payouts available
        return new Money(0);
    }

    public function totalRevenue(): Money
    {
        return Money::sum(
            $this->salesRevenue(),
            $this->leadsRevenue(),
            $this->holdsRevenue()
        );
    }

    public function setCampaigns($campaigns): CampaignGroupStatistics
    {
        $this->campaigns = $campaigns;
        return $this;
    }
}