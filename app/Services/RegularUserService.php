<?php
/**
 * User: Grzegorz
 * Date: 2016-11-28
 * Time: 23:10
 */

namespace App\Services;


use App\Entities\User;
use App\Repositories\UserRepository;

/**
 * Class RegularUserService
 * @package App\Services
 */
class RegularUserService
{

    /**
     * @var UserRepository
     */
    private $userRepository;


    /**
     * RegularUserService constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function register(array $data, bool $active = false): User
    {
        $user = $this->userRepository->create([
            'first_name' => array_get($data, 'first_name'),
            'last_name'  => array_get($data, 'last_name'),
            'email'      => array_get($data, 'email'),
            'password'   => array_get($data, 'password'),
            'active'     => $active,
            'token'      => strtoupper(str_random(8))
        ]);

        if ($active == false) {
            $user->activation_token = str_random(32);
            $user->save();
        }

        return $user;
    }
}