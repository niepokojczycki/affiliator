<?php
/**
 * User: Grzegorz
 * Date: 2016-11-28
 * Time: 21:52
 */

namespace App\Services;


use App\Entities\User;
use App\Repositories\SocialAccountRepository;
use App\Repositories\UserRepository;

use Laravel\Socialite\Contracts\User as ProviderUser;

/**
 * Class SocialAccountService
 * @package App\Services
 */
class SocialAccountService
{

    /**
     * @var SocialAccountRepository
     */
    private $socialAccountRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var RegularUserService
     */
    private $regularUserService;


    /**
     * SocialAccountService constructor.
     * @param SocialAccountRepository $socialAccountRepository
     * @param UserRepository $userRepository
     * @param RegularUserService $regularUserService
     */
    public function __construct(SocialAccountRepository $socialAccountRepository, UserRepository $userRepository, RegularUserService $regularUserService)
    {
        $this->socialAccountRepository = $socialAccountRepository;
        $this->userRepository = $userRepository;
        $this->regularUserService = $regularUserService;
    }

    public function createOrGetUser(ProviderUser $providerUser,  string $provider = 'facebook'): User
    {
        $account = $this->socialAccountRepository->getAccount($providerUser->getId(), $provider);

        if ($account)
            return $account->user;

        $account = $this->socialAccountRepository->createAccount($providerUser->getId(), $provider);

        $user = $this->findOrCreateUser($providerUser, $provider);

        $account->user()->associate($user);
        $account->save();

        if ($avatar = $providerUser->getAvatar()) {
            $user->avatar = $avatar;
            $user->save();
        }

        return $user;
    }

    public function getOrGenerateEmail(ProviderUser $providerUser, string $provider): string
    {
        $email = $providerUser->getEmail();

        if (!$email)
            $email = $providerUser->getId() . '@' . $provider . '.com';

        return $email;
    }

    public function findOrCreateUser(ProviderUser $providerUser, string $provider): User
    {
        $user = $this->findUser($providerUser);

        if (!$user)
            $user = $this->createUser($providerUser, $provider);

        return $user;
    }

    /**
     * @param ProviderUser $providerUser
     * @return User|null
     */
    protected function findUser(ProviderUser $providerUser)
    {
        return $this->userRepository->findByField('email', $providerUser->getEmail())->first();
    }

    protected function createUser(ProviderUser $providerUser, string $provider): User
    {
        $userNameArray = explode(' ', $providerUser->getName());

        $userData = [
            'first_name' => array_shift($userNameArray),
            'last_name'  => array_shift($userNameArray),
            'email'      => $this->getOrGenerateEmail($providerUser, $provider),
            'password'   => str_random(8)
        ];

        return $this->regularUserService->register($userData, true);
    }
}