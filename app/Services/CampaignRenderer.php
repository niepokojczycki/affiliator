<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 29.03.17
 * Time: 10:09
 */

namespace App\Services;


use App\Entities\Campaign;
use App\Entities\CampaignClick;
use App\Entities\Landing;
use App\Entities\LandingInterface;
use App\Repositories\CampaignRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CampaignRenderer
{
    private $landing;
    private $prelanding;
    private $campaignRepository;
    private $campaign;

    public function __construct(CampaignRepository $campaignRepository)
    {
        $this->campaignRepository = $campaignRepository;
    }

    public function render($uuid, Landing $landing = null)
    {
        $this->setCampaign($uuid);

        if ($landing)
            return $this->renderLandingDirectly($landing);

        return $this->renderContents($this->selectLandings());
    }

    protected function setCampaign(string $uuid)
    {
        $this->campaign = $this->campaignRepository->getForRendering($uuid);

        if (is_null($this->campaign))
            throw (new ModelNotFoundException())->setModel(Campaign::class);
    }

    protected function renderContents(LandingInterface $landing)
    {
        CampaignClick::incrementOrCreate($this->campaign);

        return $landing->setParams(['campaignId' => $this->campaign->uuid])->render();
    }

    protected function renderLandingDirectly(Landing $landing)
    {
        $this->landing = $landing;
        return $this->renderContents($landing);
    }

    protected function selectLandings(): LandingInterface
    {
        $this->landing = $selected = $this->campaign->landings->random();

        $prelandings = $this->campaign->prelandings;

        if ($prelandings->count())
            $this->prelanding = $selected = $prelandings->random();

        return $selected;
    }
}