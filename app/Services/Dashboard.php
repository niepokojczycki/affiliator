<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 30.03.17
 * Time: 10:21
 */

namespace App\Services;


use App\Repositories\CampaignRepository;
use App\ViewModels\ConversionRatio;
use App\ViewModels\DashboardElement;
use Carbon\Carbon;
use Illuminate\Support\Collection as SupportCollection;
use Illuminate\Support\Collection;

class Dashboard
{
    protected $dates;
    protected $revenueByDay;

    public function __construct(CampaignRepository $campaignRepository, CampaignGroupStatistics $groupStatistics)
    {
        $campaigns = $campaignRepository->getForDashboard(auth()->user());
        $this->groupStatistics = $groupStatistics->setCampaigns($campaigns);
    }

    public function getMainChartLabels(): array
    {
        return $this->getDates()->map->format('d.m')->all();
    }

    private function getDates(): SupportCollection
    {
        if (is_null($this->dates)) {
            $now = Carbon::now()->startOfDay();
            $dates = collect();

            for ($i = 0; $i < 7; $i++)
                $dates->prepend($now->copy()->subDay($i));

            $this->dates = $dates;
        }

        return $this->dates;
    }

    public function getRevenueCountByDay(): array
    {
        if (is_null($this->revenueByDay)) {
            $revenueByDay = [];
            foreach ($this->getDates() as $date) {

                $this->groupStatistics->setDateFrom($date)->setDateTo($date->copy()->endOfDay());
                $revenue = $this->groupStatistics->totalRevenue();

                $revenueByDay[$date->format('m.d')] = $revenue->getInRawDollars();
            }
            $revenueByDay = array_values($revenueByDay);
            $this->revenueByDay = $revenueByDay;
        }
        return $this->revenueByDay;
    }

    public function getDashboardElements(): SupportCollection
    {
        $this->setDatesForWeek();

        $elements = collect();

        $elements->push(new DashboardElement("Clicks", $this->groupStatistics->clicks()));
        $elements->push(new DashboardElement("Conversions", $this->groupStatistics->sales()));
        $elements->push(new DashboardElement("Revenue", $this->groupStatistics->totalRevenue()->getAmount(), true, true));

        $this->setDatesForYear();
        $elements->push(new DashboardElement("Revenue " . Carbon::now()->year, $this->groupStatistics->totalRevenue()->getAmount(), false, true));

        return $elements;
    }

    public function getConversionRatios(): SupportCollection
    {
        $this->setDatesForYear();

        $leadsCount = $this->groupStatistics->leads();
        $salesCount = $this->groupStatistics->sales();
        $awaitingCount = $this->groupStatistics->awaiting();
        $rejectedCount = $this->groupStatistics->rejects();

        $ratios = collect();

        $ratios->push(new ConversionRatio("Sold", $salesCount, $leadsCount));
        $ratios->push(new ConversionRatio("Awaiting", $awaitingCount, $leadsCount));
        $ratios->push(new ConversionRatio("Rejected", $rejectedCount, $leadsCount));

        return $ratios;
    }

    public function getClicksCountByDay()
    {
        /** @var Collection $clicksByDay */
        $clicksByDay = $this->groupStatistics->getClicks()->groupBy(function ($item) {
            return Carbon::createFromTimestamp($item->timestamp)->format('m.d');
        });

        $data = [];

        foreach ($this->getDates() as $date) {
            $formattedDate = $date->format('m.d');

            $clicks = $clicksByDay->first(function ($item, $key) use ($formattedDate) {
                return $key == $formattedDate;
            });

            $data[$formattedDate] = $clicks ? $clicks->sum('clicks_count') : 0;
        }

        return array_values($data);
    }

    private function setStatisticsDates(Carbon $dateFrom = null, Carbon $dateTo = null)
    {
        $this->groupStatistics->setDateFrom($dateFrom)->setDateTo($dateTo);
        return $this;
    }

    private function setDatesForWeek()
    {
        $dateTo = Carbon::now()->endOfDay();
        $dateFrom = $dateTo->copy()->subWeek();
        $this->setStatisticsDates($dateFrom, $dateTo);
    }

    private function setDatesForYear()
    {
        $dateFrom = Carbon::now()->startOfYear();
        $this->setStatisticsDates($dateFrom);
    }
}