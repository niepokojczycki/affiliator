<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 05.04.17
 * Time: 14:22
 */

namespace App\Services;


use Illuminate\Support\Facades\Response;

class CSVDownloader
{

    public function download(string $filename, array $data)
    {
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=' . $filename . '',
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];

        return Response::stream($this->callback($data), 200, $headers);
    }

    /**
     * @param array $data
     * @return \Closure
     */
    private function callback(array $data): \Closure
    {
        return function () use ($data) {
            $FH = fopen('php://output', 'w');
            foreach ($data as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };
    }
}