<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 03.04.17
 * Time: 16:32
 */

namespace App\Services;


use App\Support\Money;

interface CampaignStatisticsInterface
{
    public function clicks(): int;

    public function leads(): int;

    public function sales(): int;

    public function awaiting(): int;

    public function rejects(): int;

    public function salesRevenue(): Money;

    public function leadsRevenue(): Money;

    public function holdsRevenue(): Money;

    public function totalRevenue(): Money;
}