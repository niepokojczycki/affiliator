<?php
/**
 * Created by PhpStorm.
 * User: krzysiek
 * Date: 03.04.17
 * Time: 14:55
 */

namespace App\Services;

use App\Entities\Campaign;
use Carbon\Carbon;

class CampaignStatistics extends CampaignGroupStatistics
{
    protected $campaign;

    public function __construct(Campaign $campaign, Carbon $dateFrom = null, Carbon $dateTo = null)
    {
        $this->campaign = $campaign;
        parent::__construct(collect()->push($campaign), $dateFrom, $dateTo);
    }

    public function epc(): float
    {
        return $this->campaign->epc;
    }
}